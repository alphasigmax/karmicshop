// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function (config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '../../',

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            // bower:js
            'main/webapp/bower_components/jquery/dist/jquery.js',
            'main/webapp/bower_components/modernizr/modernizr.js',
            'main/webapp/bower_components/angular/angular.js',
            'main/webapp/bower_components/angular-aria/angular-aria.js',
            'main/webapp/bower_components/angular-cache-buster/angular-cache-buster.js',
            'main/webapp/bower_components/angular-dynamic-locale/src/tmhDynamicLocale.js',
            'main/webapp/bower_components/angular-local-storage/dist/angular-local-storage.js',
            'main/webapp/bower_components/angular-translate/angular-translate.js',
            'main/webapp/bower_components/messageformat/messageformat.js',
            'main/webapp/bower_components/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.js',
            'main/webapp/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
            'main/webapp/bower_components/angular-cookies/angular-cookies.js',
            'main/webapp/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
            'main/webapp/bower_components/json3/lib/json3.js',
            'main/webapp/bower_components/ng-file-upload/ng-file-upload.js',
            'main/webapp/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
            'main/webapp/bower_components/angular-animate/angular-animate.js',
            'main/webapp/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'main/webapp/bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
            'main/webapp/bower_components/Chart.js/Chart.js',
            'main/webapp/bower_components/angular-chart.js/dist/angular-chart.js',
            'main/webapp/bower_components/angular-file-upload/dist/angular-file-upload.min.js',
            'main/webapp/bower_components/angular-inview/angular-inview.js',
            'main/webapp/bower_components/angular-loading-bar/build/loading-bar.js',
            'main/webapp/bower_components/angular-notify/dist/angular-notify.js',
            'main/webapp/bower_components/perfect-scrollbar/src/perfect-scrollbar.js',
            'main/webapp/bower_components/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js',
            'main/webapp/bower_components/angular-resource/angular-resource.js',
            'main/webapp/bower_components/rickshaw/rickshaw.js',
            'main/webapp/bower_components/d3/d3.js',
            'main/webapp/bower_components/angular-rickshaw/rickshaw.js',
            'main/webapp/bower_components/angular-sanitize/angular-sanitize.js',
            'main/webapp/bower_components/angular-smart-table/dist/smart-table.js',
            'main/webapp/bower_components/angular-touch/angular-touch.js',
            'main/webapp/bower_components/moment/moment.js',
            'main/webapp/bower_components/fullcalendar/dist/fullcalendar.js',
            'main/webapp/bower_components/angular-ui-calendar/src/calendar.js',
            'main/webapp/bower_components/angular-ui-grid/ui-grid.js',
            'main/webapp/bower_components/angular-ui-router/release/angular-ui-router.js',
            'main/webapp/bower_components/angular-ui-utils/ui-utils.js',
            'main/webapp/bower_components/angular-xeditable/dist/js/xeditable.js',
            'main/webapp/bower_components/bootstrap/dist/js/bootstrap.js',
            'main/webapp/bower_components/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
            'main/webapp/bower_components/bootstrap-daterangepicker/daterangepicker.js',
            'main/webapp/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
            'main/webapp/bower_components/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
            'main/webapp/bower_components/chosen/chosen.jquery.min.js',
            'main/webapp/bower_components/datatables/media/js/jquery.dataTables.js',
            'main/webapp/bower_components/footable/js/footable.js',
            'main/webapp/bower_components/flot/jquery.flot.js',
            'main/webapp/bower_components/flot.tooltip/js/jquery.flot.tooltip.js',
            'main/webapp/bower_components/flot-spline/js/jquery.flot.spline.js',
            'main/webapp/bower_components/html5sortable/jquery.sortable.js',
            'main/webapp/bower_components/requirejs/require.js',
            'main/webapp/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.js',
            'main/webapp/bower_components/jquery-knob/js/jquery.knob.js',
            'main/webapp/bower_components/ng-grid/build/ng-grid.js',
            'main/webapp/bower_components/ngImgCrop/compile/minified/ng-img-crop.js',
            'main/webapp/bower_components/raphael/raphael.js',
            'main/webapp/bower_components/mocha/mocha.js',
            'main/webapp/bower_components/morrisjs/morris.js',
            'main/webapp/bower_components/ngmorris/src/ngMorris.js',
            'main/webapp/bower_components/ngstorage/ngStorage.js',
            'main/webapp/bower_components/oclazyload/dist/ocLazyLoad.js',
            'main/webapp/bower_components/venturocket-angular-slider/build/angular-slider.js',
            'main/webapp/bower_components/angular-mocks/angular-mocks.js',
            // endbower
            'main/webapp/scripts/app/app.js',
            'main/webapp/scripts/app/**/*.js',
            'main/webapp/scripts/components/**/*.{js,html}',
            'test/javascript/**/!(karma.conf).js'
        ],


        // list of files / patterns to exclude
        exclude: [],

        preprocessors: {
            './**/*.js': ['coverage']
        },

        reporters: ['dots', 'jenkins', 'coverage', 'progress'],

        jenkinsReporter: {
            
            outputFile: '../build/test-results/karma/TESTS-results.xml'
        },

        coverageReporter: {
            
            dir: '../build/test-results/coverage',
            reporters: [
                {type: 'lcov', subdir: 'report-lcov'}
            ]
        },

        // web server port
        port: 9876,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        // to avoid DISCONNECTED messages when connecting to slow virtual machines
        browserDisconnectTimeout : 10000, // default 2000
        browserDisconnectTolerance : 1, // default 0
        browserNoActivityTimeout : 4*60*1000 //default 10000
    });
};
