package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.Seller;
import com.karrmic.repository.SellerRepository;
import com.karrmic.repository.search.SellerSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SellerResource REST controller.
 *
 * @see SellerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SellerResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_FULL_NAME = "AAA";
    private static final String UPDATED_FULL_NAME = "BBB";
    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBB";

    private static final DateTime DEFAULT_REGISTER_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_REGISTER_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_REGISTER_DATE_STR = dateTimeFormatter.print(DEFAULT_REGISTER_DATE);

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;
    private static final String DEFAULT_SELLER_INFO = "AAAAA";
    private static final String UPDATED_SELLER_INFO = "BBBBB";

    private static final Integer DEFAULT_KARMA_POINTS = 1;
    private static final Integer UPDATED_KARMA_POINTS = 2;

    @Inject
    private SellerRepository sellerRepository;

    @Inject
    private SellerSearchRepository sellerSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSellerMockMvc;

    private Seller seller;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SellerResource sellerResource = new SellerResource();
        ReflectionTestUtils.setField(sellerResource, "sellerRepository", sellerRepository);
        ReflectionTestUtils.setField(sellerResource, "sellerSearchRepository", sellerSearchRepository);
        this.restSellerMockMvc = MockMvcBuilders.standaloneSetup(sellerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        seller = new Seller();
        seller.setFullName(DEFAULT_FULL_NAME);
        seller.setEmailAddress(DEFAULT_EMAIL_ADDRESS);
        seller.setRegisterDate(DEFAULT_REGISTER_DATE);
        seller.setIsActive(DEFAULT_IS_ACTIVE);
        seller.setSellerInfo(DEFAULT_SELLER_INFO);
        seller.setKarmaPoints(DEFAULT_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void createSeller() throws Exception {
        int databaseSizeBeforeCreate = sellerRepository.findAll().size();

        // Create the Seller

        restSellerMockMvc.perform(post("/api/sellers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seller)))
                .andExpect(status().isCreated());

        // Validate the Seller in the database
        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeCreate + 1);
        Seller testSeller = sellers.get(sellers.size() - 1);
        assertThat(testSeller.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testSeller.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testSeller.getRegisterDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_REGISTER_DATE);
        assertThat(testSeller.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testSeller.getSellerInfo()).isEqualTo(DEFAULT_SELLER_INFO);
        assertThat(testSeller.getKarmaPoints()).isEqualTo(DEFAULT_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void checkFullNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = sellerRepository.findAll().size();
        // set the field null
        seller.setFullName(null);

        // Create the Seller, which fails.

        restSellerMockMvc.perform(post("/api/sellers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seller)))
                .andExpect(status().isBadRequest());

        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = sellerRepository.findAll().size();
        // set the field null
        seller.setEmailAddress(null);

        // Create the Seller, which fails.

        restSellerMockMvc.perform(post("/api/sellers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seller)))
                .andExpect(status().isBadRequest());

        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRegisterDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = sellerRepository.findAll().size();
        // set the field null
        seller.setRegisterDate(null);

        // Create the Seller, which fails.

        restSellerMockMvc.perform(post("/api/sellers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seller)))
                .andExpect(status().isBadRequest());

        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSellers() throws Exception {
        // Initialize the database
        sellerRepository.saveAndFlush(seller);

        // Get all the sellers
        restSellerMockMvc.perform(get("/api/sellers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(seller.getId().intValue())))
                .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
                .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].registerDate").value(hasItem(DEFAULT_REGISTER_DATE_STR)))
                .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].sellerInfo").value(hasItem(DEFAULT_SELLER_INFO.toString())))
                .andExpect(jsonPath("$.[*].karmaPoints").value(hasItem(DEFAULT_KARMA_POINTS)));
    }

    @Test
    @Transactional
    public void getSeller() throws Exception {
        // Initialize the database
        sellerRepository.saveAndFlush(seller);

        // Get the seller
        restSellerMockMvc.perform(get("/api/sellers/{id}", seller.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(seller.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.registerDate").value(DEFAULT_REGISTER_DATE_STR))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.sellerInfo").value(DEFAULT_SELLER_INFO.toString()))
            .andExpect(jsonPath("$.karmaPoints").value(DEFAULT_KARMA_POINTS));
    }

    @Test
    @Transactional
    public void getNonExistingSeller() throws Exception {
        // Get the seller
        restSellerMockMvc.perform(get("/api/sellers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSeller() throws Exception {
        // Initialize the database
        sellerRepository.saveAndFlush(seller);

		int databaseSizeBeforeUpdate = sellerRepository.findAll().size();

        // Update the seller
        seller.setFullName(UPDATED_FULL_NAME);
        seller.setEmailAddress(UPDATED_EMAIL_ADDRESS);
        seller.setRegisterDate(UPDATED_REGISTER_DATE);
        seller.setIsActive(UPDATED_IS_ACTIVE);
        seller.setSellerInfo(UPDATED_SELLER_INFO);
        seller.setKarmaPoints(UPDATED_KARMA_POINTS);

        restSellerMockMvc.perform(put("/api/sellers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(seller)))
                .andExpect(status().isOk());

        // Validate the Seller in the database
        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeUpdate);
        Seller testSeller = sellers.get(sellers.size() - 1);
        assertThat(testSeller.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testSeller.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testSeller.getRegisterDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_REGISTER_DATE);
        assertThat(testSeller.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testSeller.getSellerInfo()).isEqualTo(UPDATED_SELLER_INFO);
        assertThat(testSeller.getKarmaPoints()).isEqualTo(UPDATED_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void deleteSeller() throws Exception {
        // Initialize the database
        sellerRepository.saveAndFlush(seller);

		int databaseSizeBeforeDelete = sellerRepository.findAll().size();

        // Get the seller
        restSellerMockMvc.perform(delete("/api/sellers/{id}", seller.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Seller> sellers = sellerRepository.findAll();
        assertThat(sellers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
