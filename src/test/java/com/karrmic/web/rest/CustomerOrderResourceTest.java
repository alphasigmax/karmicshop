package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.CustomerOrder;
import com.karrmic.repository.CustomerOrderRepository;
import com.karrmic.repository.search.CustomerOrderSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.karrmic.domain.enumeration.OrderStatus;

/**
 * Test class for the CustomerOrderResource REST controller.
 *
 * @see CustomerOrderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CustomerOrderResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final Double DEFAULT_AMOUNT_PAID = 0D;
    private static final Double UPDATED_AMOUNT_PAID = 1D;

    private static final DateTime DEFAULT_DATE_CREATED = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_DATE_CREATED = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_DATE_CREATED_STR = dateTimeFormatter.print(DEFAULT_DATE_CREATED);
    private static final String DEFAULT_TRANSACTION_NUMBER = "AAAAA";
    private static final String UPDATED_TRANSACTION_NUMBER = "BBBBB";


private static final OrderStatus DEFAULT_ORDER_STATUS = OrderStatus.INCART;
    private static final OrderStatus UPDATED_ORDER_STATUS = OrderStatus.STARTCHECKOUT;

    private static final Long DEFAULT_PRODUCT_ID = 1L;
    private static final Long UPDATED_PRODUCT_ID = 2L;

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;
    private static final String DEFAULT_SHOPPER_USER_ID = "AAAAA";
    private static final String UPDATED_SHOPPER_USER_ID = "BBBBB";

    @Inject
    private CustomerOrderRepository customerOrderRepository;

    @Inject
    private CustomerOrderSearchRepository customerOrderSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCustomerOrderMockMvc;

    private CustomerOrder customerOrder;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CustomerOrderResource customerOrderResource = new CustomerOrderResource();
        ReflectionTestUtils.setField(customerOrderResource, "customerOrderRepository", customerOrderRepository);
        ReflectionTestUtils.setField(customerOrderResource, "customerOrderSearchRepository", customerOrderSearchRepository);
        this.restCustomerOrderMockMvc = MockMvcBuilders.standaloneSetup(customerOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        customerOrder = new CustomerOrder();
        customerOrder.setAmountPaid(DEFAULT_AMOUNT_PAID);
        customerOrder.setDateCreated(DEFAULT_DATE_CREATED);
        customerOrder.setTransactionNumber(DEFAULT_TRANSACTION_NUMBER);
        customerOrder.setOrderStatus(DEFAULT_ORDER_STATUS);
        customerOrder.setProductId(DEFAULT_PRODUCT_ID);
        customerOrder.setQuantity(DEFAULT_QUANTITY);
        customerOrder.setShopperUserId(DEFAULT_SHOPPER_USER_ID);
    }

    @Test
    @Transactional
    public void createCustomerOrder() throws Exception {
        int databaseSizeBeforeCreate = customerOrderRepository.findAll().size();

        // Create the CustomerOrder

        restCustomerOrderMockMvc.perform(post("/api/customerOrders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customerOrder)))
                .andExpect(status().isCreated());

        // Validate the CustomerOrder in the database
        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeCreate + 1);
        CustomerOrder testCustomerOrder = customerOrders.get(customerOrders.size() - 1);
        assertThat(testCustomerOrder.getAmountPaid()).isEqualTo(DEFAULT_AMOUNT_PAID);
        assertThat(testCustomerOrder.getDateCreated().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testCustomerOrder.getTransactionNumber()).isEqualTo(DEFAULT_TRANSACTION_NUMBER);
        assertThat(testCustomerOrder.getOrderStatus()).isEqualTo(DEFAULT_ORDER_STATUS);
        assertThat(testCustomerOrder.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
        assertThat(testCustomerOrder.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testCustomerOrder.getShopperUserId()).isEqualTo(DEFAULT_SHOPPER_USER_ID);
    }

    @Test
    @Transactional
    public void checkAmountPaidIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerOrderRepository.findAll().size();
        // set the field null
        customerOrder.setAmountPaid(null);

        // Create the CustomerOrder, which fails.

        restCustomerOrderMockMvc.perform(post("/api/customerOrders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customerOrder)))
                .andExpect(status().isBadRequest());

        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateCreatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerOrderRepository.findAll().size();
        // set the field null
        customerOrder.setDateCreated(null);

        // Create the CustomerOrder, which fails.

        restCustomerOrderMockMvc.perform(post("/api/customerOrders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customerOrder)))
                .andExpect(status().isBadRequest());

        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrderStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerOrderRepository.findAll().size();
        // set the field null
        customerOrder.setOrderStatus(null);

        // Create the CustomerOrder, which fails.

        restCustomerOrderMockMvc.perform(post("/api/customerOrders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customerOrder)))
                .andExpect(status().isBadRequest());

        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCustomerOrders() throws Exception {
        // Initialize the database
        customerOrderRepository.saveAndFlush(customerOrder);

        // Get all the customerOrders
        restCustomerOrderMockMvc.perform(get("/api/customerOrders"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customerOrder.getId().intValue())))
                .andExpect(jsonPath("$.[*].amountPaid").value(hasItem(DEFAULT_AMOUNT_PAID.doubleValue())))
                .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(DEFAULT_DATE_CREATED_STR)))
                .andExpect(jsonPath("$.[*].transactionNumber").value(hasItem(DEFAULT_TRANSACTION_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].orderStatus").value(hasItem(DEFAULT_ORDER_STATUS.toString())))
                .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID.intValue())))
                .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
                .andExpect(jsonPath("$.[*].shopperUserId").value(hasItem(DEFAULT_SHOPPER_USER_ID.toString())));
    }

    @Test
    @Transactional
    public void getCustomerOrder() throws Exception {
        // Initialize the database
        customerOrderRepository.saveAndFlush(customerOrder);

        // Get the customerOrder
        restCustomerOrderMockMvc.perform(get("/api/customerOrders/{id}", customerOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(customerOrder.getId().intValue()))
            .andExpect(jsonPath("$.amountPaid").value(DEFAULT_AMOUNT_PAID.doubleValue()))
            .andExpect(jsonPath("$.dateCreated").value(DEFAULT_DATE_CREATED_STR))
            .andExpect(jsonPath("$.transactionNumber").value(DEFAULT_TRANSACTION_NUMBER.toString()))
            .andExpect(jsonPath("$.orderStatus").value(DEFAULT_ORDER_STATUS.toString()))
            .andExpect(jsonPath("$.productId").value(DEFAULT_PRODUCT_ID.intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.shopperUserId").value(DEFAULT_SHOPPER_USER_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomerOrder() throws Exception {
        // Get the customerOrder
        restCustomerOrderMockMvc.perform(get("/api/customerOrders/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomerOrder() throws Exception {
        // Initialize the database
        customerOrderRepository.saveAndFlush(customerOrder);

		int databaseSizeBeforeUpdate = customerOrderRepository.findAll().size();

        // Update the customerOrder
        customerOrder.setAmountPaid(UPDATED_AMOUNT_PAID);
        customerOrder.setDateCreated(UPDATED_DATE_CREATED);
        customerOrder.setTransactionNumber(UPDATED_TRANSACTION_NUMBER);
        customerOrder.setOrderStatus(UPDATED_ORDER_STATUS);
        customerOrder.setProductId(UPDATED_PRODUCT_ID);
        customerOrder.setQuantity(UPDATED_QUANTITY);
        customerOrder.setShopperUserId(UPDATED_SHOPPER_USER_ID);

        restCustomerOrderMockMvc.perform(put("/api/customerOrders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customerOrder)))
                .andExpect(status().isOk());

        // Validate the CustomerOrder in the database
        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeUpdate);
        CustomerOrder testCustomerOrder = customerOrders.get(customerOrders.size() - 1);
        assertThat(testCustomerOrder.getAmountPaid()).isEqualTo(UPDATED_AMOUNT_PAID);
        assertThat(testCustomerOrder.getDateCreated().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testCustomerOrder.getTransactionNumber()).isEqualTo(UPDATED_TRANSACTION_NUMBER);
        assertThat(testCustomerOrder.getOrderStatus()).isEqualTo(UPDATED_ORDER_STATUS);
        assertThat(testCustomerOrder.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
        assertThat(testCustomerOrder.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testCustomerOrder.getShopperUserId()).isEqualTo(UPDATED_SHOPPER_USER_ID);
    }

    @Test
    @Transactional
    public void deleteCustomerOrder() throws Exception {
        // Initialize the database
        customerOrderRepository.saveAndFlush(customerOrder);

		int databaseSizeBeforeDelete = customerOrderRepository.findAll().size();

        // Get the customerOrder
        restCustomerOrderMockMvc.perform(delete("/api/customerOrders/{id}", customerOrder.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CustomerOrder> customerOrders = customerOrderRepository.findAll();
        assertThat(customerOrders).hasSize(databaseSizeBeforeDelete - 1);
    }
}
