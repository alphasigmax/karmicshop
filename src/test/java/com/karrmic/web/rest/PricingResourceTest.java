package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.Pricing;
import com.karrmic.repository.PricingRepository;
import com.karrmic.repository.search.PricingSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.karrmic.domain.enumeration.PricingType;

/**
 * Test class for the PricingResource REST controller.
 *
 * @see PricingResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PricingResourceTest {


    private static final Double DEFAULT_ORIGINAL_PRICE = 0D;
    private static final Double UPDATED_ORIGINAL_PRICE = 1D;

    private static final Double DEFAULT_CURRENT_PRICE = 0D;
    private static final Double UPDATED_CURRENT_PRICE = 1D;


private static final PricingType DEFAULT_PRICING_TYPE = PricingType.NORMAL;
    private static final PricingType UPDATED_PRICING_TYPE = PricingType.DISCOUNTED;

    @Inject
    private PricingRepository pricingRepository;

    @Inject
    private PricingSearchRepository pricingSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPricingMockMvc;

    private Pricing pricing;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PricingResource pricingResource = new PricingResource();
        ReflectionTestUtils.setField(pricingResource, "pricingRepository", pricingRepository);
        ReflectionTestUtils.setField(pricingResource, "pricingSearchRepository", pricingSearchRepository);
        this.restPricingMockMvc = MockMvcBuilders.standaloneSetup(pricingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        pricing = new Pricing();
        pricing.setOriginalPrice(DEFAULT_ORIGINAL_PRICE);
        pricing.setCurrentPrice(DEFAULT_CURRENT_PRICE);
        pricing.setPricingType(DEFAULT_PRICING_TYPE);
    }

    @Test
    @Transactional
    public void createPricing() throws Exception {
        int databaseSizeBeforeCreate = pricingRepository.findAll().size();

        // Create the Pricing

        restPricingMockMvc.perform(post("/api/pricings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pricing)))
                .andExpect(status().isCreated());

        // Validate the Pricing in the database
        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeCreate + 1);
        Pricing testPricing = pricings.get(pricings.size() - 1);
        assertThat(testPricing.getOriginalPrice()).isEqualTo(DEFAULT_ORIGINAL_PRICE);
        assertThat(testPricing.getCurrentPrice()).isEqualTo(DEFAULT_CURRENT_PRICE);
        assertThat(testPricing.getPricingType()).isEqualTo(DEFAULT_PRICING_TYPE);
    }

    @Test
    @Transactional
    public void checkOriginalPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricingRepository.findAll().size();
        // set the field null
        pricing.setOriginalPrice(null);

        // Create the Pricing, which fails.

        restPricingMockMvc.perform(post("/api/pricings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pricing)))
                .andExpect(status().isBadRequest());

        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCurrentPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricingRepository.findAll().size();
        // set the field null
        pricing.setCurrentPrice(null);

        // Create the Pricing, which fails.

        restPricingMockMvc.perform(post("/api/pricings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pricing)))
                .andExpect(status().isBadRequest());

        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPricingTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricingRepository.findAll().size();
        // set the field null
        pricing.setPricingType(null);

        // Create the Pricing, which fails.

        restPricingMockMvc.perform(post("/api/pricings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pricing)))
                .andExpect(status().isBadRequest());

        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPricings() throws Exception {
        // Initialize the database
        pricingRepository.saveAndFlush(pricing);

        // Get all the pricings
        restPricingMockMvc.perform(get("/api/pricings"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pricing.getId().intValue())))
                .andExpect(jsonPath("$.[*].originalPrice").value(hasItem(DEFAULT_ORIGINAL_PRICE.doubleValue())))
                .andExpect(jsonPath("$.[*].currentPrice").value(hasItem(DEFAULT_CURRENT_PRICE.doubleValue())))
                .andExpect(jsonPath("$.[*].pricingType").value(hasItem(DEFAULT_PRICING_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPricing() throws Exception {
        // Initialize the database
        pricingRepository.saveAndFlush(pricing);

        // Get the pricing
        restPricingMockMvc.perform(get("/api/pricings/{id}", pricing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(pricing.getId().intValue()))
            .andExpect(jsonPath("$.originalPrice").value(DEFAULT_ORIGINAL_PRICE.doubleValue()))
            .andExpect(jsonPath("$.currentPrice").value(DEFAULT_CURRENT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.pricingType").value(DEFAULT_PRICING_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPricing() throws Exception {
        // Get the pricing
        restPricingMockMvc.perform(get("/api/pricings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePricing() throws Exception {
        // Initialize the database
        pricingRepository.saveAndFlush(pricing);

		int databaseSizeBeforeUpdate = pricingRepository.findAll().size();

        // Update the pricing
        pricing.setOriginalPrice(UPDATED_ORIGINAL_PRICE);
        pricing.setCurrentPrice(UPDATED_CURRENT_PRICE);
        pricing.setPricingType(UPDATED_PRICING_TYPE);

        restPricingMockMvc.perform(put("/api/pricings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pricing)))
                .andExpect(status().isOk());

        // Validate the Pricing in the database
        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeUpdate);
        Pricing testPricing = pricings.get(pricings.size() - 1);
        assertThat(testPricing.getOriginalPrice()).isEqualTo(UPDATED_ORIGINAL_PRICE);
        assertThat(testPricing.getCurrentPrice()).isEqualTo(UPDATED_CURRENT_PRICE);
        assertThat(testPricing.getPricingType()).isEqualTo(UPDATED_PRICING_TYPE);
    }

    @Test
    @Transactional
    public void deletePricing() throws Exception {
        // Initialize the database
        pricingRepository.saveAndFlush(pricing);

		int databaseSizeBeforeDelete = pricingRepository.findAll().size();

        // Get the pricing
        restPricingMockMvc.perform(delete("/api/pricings/{id}", pricing.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Pricing> pricings = pricingRepository.findAll();
        assertThat(pricings).hasSize(databaseSizeBeforeDelete - 1);
    }
}
