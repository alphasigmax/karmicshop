package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.City;
import com.karrmic.repository.CityRepository;
import com.karrmic.repository.search.CitySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CityResource REST controller.
 *
 * @see CityResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CityResourceTest {

    private static final String DEFAULT_GEONAME_ID = "AAAAA";
    private static final String UPDATED_GEONAME_ID = "BBBBB";
    private static final String DEFAULT_CONTINENT_CODE = "AAAAA";
    private static final String UPDATED_CONTINENT_CODE = "BBBBB";
    private static final String DEFAULT_COUNTRY_ISO_CODE = "AAAAA";
    private static final String UPDATED_COUNTRY_ISO_CODE = "BBBBB";
    private static final String DEFAULT_CITY_NAME = "AAAAA";
    private static final String UPDATED_CITY_NAME = "BBBBB";
    private static final String DEFAULT_TIME_ZONE = "AAAAA";
    private static final String UPDATED_TIME_ZONE = "BBBBB";

    @Inject
    private CityRepository cityRepository;

    @Inject
    private CitySearchRepository citySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCityMockMvc;

    private City city;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CityResource cityResource = new CityResource();
        ReflectionTestUtils.setField(cityResource, "cityRepository", cityRepository);
        ReflectionTestUtils.setField(cityResource, "citySearchRepository", citySearchRepository);
        this.restCityMockMvc = MockMvcBuilders.standaloneSetup(cityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        city = new City();
        city.setGeonameId(DEFAULT_GEONAME_ID);
        city.setContinentCode(DEFAULT_CONTINENT_CODE);
        city.setCountryIsoCode(DEFAULT_COUNTRY_ISO_CODE);
        city.setCityName(DEFAULT_CITY_NAME);
        city.setTimeZone(DEFAULT_TIME_ZONE);
    }

    @Test
    @Transactional
    public void createCity() throws Exception {
        int databaseSizeBeforeCreate = cityRepository.findAll().size();

        // Create the City

        restCityMockMvc.perform(post("/api/citys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(city)))
                .andExpect(status().isCreated());

        // Validate the City in the database
        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeCreate + 1);
        City testCity = citys.get(citys.size() - 1);
        assertThat(testCity.getGeonameId()).isEqualTo(DEFAULT_GEONAME_ID);
        assertThat(testCity.getContinentCode()).isEqualTo(DEFAULT_CONTINENT_CODE);
        assertThat(testCity.getCountryIsoCode()).isEqualTo(DEFAULT_COUNTRY_ISO_CODE);
        assertThat(testCity.getCityName()).isEqualTo(DEFAULT_CITY_NAME);
        assertThat(testCity.getTimeZone()).isEqualTo(DEFAULT_TIME_ZONE);
    }

    @Test
    @Transactional
    public void checkGeonameIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = cityRepository.findAll().size();
        // set the field null
        city.setGeonameId(null);

        // Create the City, which fails.

        restCityMockMvc.perform(post("/api/citys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(city)))
                .andExpect(status().isBadRequest());

        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContinentCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cityRepository.findAll().size();
        // set the field null
        city.setContinentCode(null);

        // Create the City, which fails.

        restCityMockMvc.perform(post("/api/citys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(city)))
                .andExpect(status().isBadRequest());

        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIsoCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cityRepository.findAll().size();
        // set the field null
        city.setCountryIsoCode(null);

        // Create the City, which fails.

        restCityMockMvc.perform(post("/api/citys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(city)))
                .andExpect(status().isBadRequest());

        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCitys() throws Exception {
        // Initialize the database
        cityRepository.saveAndFlush(city);

        // Get all the citys
        restCityMockMvc.perform(get("/api/citys"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(city.getId().intValue())))
                .andExpect(jsonPath("$.[*].geonameId").value(hasItem(DEFAULT_GEONAME_ID.toString())))
                .andExpect(jsonPath("$.[*].continentCode").value(hasItem(DEFAULT_CONTINENT_CODE.toString())))
                .andExpect(jsonPath("$.[*].countryIsoCode").value(hasItem(DEFAULT_COUNTRY_ISO_CODE.toString())))
                .andExpect(jsonPath("$.[*].cityName").value(hasItem(DEFAULT_CITY_NAME.toString())))
                .andExpect(jsonPath("$.[*].timeZone").value(hasItem(DEFAULT_TIME_ZONE.toString())));
    }

    @Test
    @Transactional
    public void getCity() throws Exception {
        // Initialize the database
        cityRepository.saveAndFlush(city);

        // Get the city
        restCityMockMvc.perform(get("/api/citys/{id}", city.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(city.getId().intValue()))
            .andExpect(jsonPath("$.geonameId").value(DEFAULT_GEONAME_ID.toString()))
            .andExpect(jsonPath("$.continentCode").value(DEFAULT_CONTINENT_CODE.toString()))
            .andExpect(jsonPath("$.countryIsoCode").value(DEFAULT_COUNTRY_ISO_CODE.toString()))
            .andExpect(jsonPath("$.cityName").value(DEFAULT_CITY_NAME.toString()))
            .andExpect(jsonPath("$.timeZone").value(DEFAULT_TIME_ZONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCity() throws Exception {
        // Get the city
        restCityMockMvc.perform(get("/api/citys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCity() throws Exception {
        // Initialize the database
        cityRepository.saveAndFlush(city);

		int databaseSizeBeforeUpdate = cityRepository.findAll().size();

        // Update the city
        city.setGeonameId(UPDATED_GEONAME_ID);
        city.setContinentCode(UPDATED_CONTINENT_CODE);
        city.setCountryIsoCode(UPDATED_COUNTRY_ISO_CODE);
        city.setCityName(UPDATED_CITY_NAME);
        city.setTimeZone(UPDATED_TIME_ZONE);

        restCityMockMvc.perform(put("/api/citys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(city)))
                .andExpect(status().isOk());

        // Validate the City in the database
        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeUpdate);
        City testCity = citys.get(citys.size() - 1);
        assertThat(testCity.getGeonameId()).isEqualTo(UPDATED_GEONAME_ID);
        assertThat(testCity.getContinentCode()).isEqualTo(UPDATED_CONTINENT_CODE);
        assertThat(testCity.getCountryIsoCode()).isEqualTo(UPDATED_COUNTRY_ISO_CODE);
        assertThat(testCity.getCityName()).isEqualTo(UPDATED_CITY_NAME);
        assertThat(testCity.getTimeZone()).isEqualTo(UPDATED_TIME_ZONE);
    }

    @Test
    @Transactional
    public void deleteCity() throws Exception {
        // Initialize the database
        cityRepository.saveAndFlush(city);

		int databaseSizeBeforeDelete = cityRepository.findAll().size();

        // Get the city
        restCityMockMvc.perform(delete("/api/citys/{id}", city.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<City> citys = cityRepository.findAll();
        assertThat(citys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
