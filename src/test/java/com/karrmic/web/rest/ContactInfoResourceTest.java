package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.ContactInfo;
import com.karrmic.repository.ContactInfoRepository;
import com.karrmic.repository.search.ContactInfoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ContactInfoResource REST controller.
 *
 * @see ContactInfoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ContactInfoResourceTest {

    private static final String DEFAULT_INFO_TYPE = "AAAAA";
    private static final String UPDATED_INFO_TYPE = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_PHONE = "AAAAA";
    private static final String UPDATED_PHONE = "BBBBB";
    private static final String DEFAULT_WEBSITE = "AAAAA";
    private static final String UPDATED_WEBSITE = "BBBBB";

    @Inject
    private ContactInfoRepository contactInfoRepository;

    @Inject
    private ContactInfoSearchRepository contactInfoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restContactInfoMockMvc;

    private ContactInfo contactInfo;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ContactInfoResource contactInfoResource = new ContactInfoResource();
        ReflectionTestUtils.setField(contactInfoResource, "contactInfoRepository", contactInfoRepository);
        ReflectionTestUtils.setField(contactInfoResource, "contactInfoSearchRepository", contactInfoSearchRepository);
        this.restContactInfoMockMvc = MockMvcBuilders.standaloneSetup(contactInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        contactInfo = new ContactInfo();
        contactInfo.setInfoType(DEFAULT_INFO_TYPE);
        contactInfo.setEmail(DEFAULT_EMAIL);
        contactInfo.setPhone(DEFAULT_PHONE);
        contactInfo.setWebsite(DEFAULT_WEBSITE);
    }

    @Test
    @Transactional
    public void createContactInfo() throws Exception {
        int databaseSizeBeforeCreate = contactInfoRepository.findAll().size();

        // Create the ContactInfo

        restContactInfoMockMvc.perform(post("/api/contactInfos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contactInfo)))
                .andExpect(status().isCreated());

        // Validate the ContactInfo in the database
        List<ContactInfo> contactInfos = contactInfoRepository.findAll();
        assertThat(contactInfos).hasSize(databaseSizeBeforeCreate + 1);
        ContactInfo testContactInfo = contactInfos.get(contactInfos.size() - 1);
        assertThat(testContactInfo.getInfoType()).isEqualTo(DEFAULT_INFO_TYPE);
        assertThat(testContactInfo.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testContactInfo.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testContactInfo.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
    }

    @Test
    @Transactional
    public void checkInfoTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactInfoRepository.findAll().size();
        // set the field null
        contactInfo.setInfoType(null);

        // Create the ContactInfo, which fails.

        restContactInfoMockMvc.perform(post("/api/contactInfos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contactInfo)))
                .andExpect(status().isBadRequest());

        List<ContactInfo> contactInfos = contactInfoRepository.findAll();
        assertThat(contactInfos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactInfos() throws Exception {
        // Initialize the database
        contactInfoRepository.saveAndFlush(contactInfo);

        // Get all the contactInfos
        restContactInfoMockMvc.perform(get("/api/contactInfos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(contactInfo.getId().intValue())))
                .andExpect(jsonPath("$.[*].infoType").value(hasItem(DEFAULT_INFO_TYPE.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
                .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())));
    }

    @Test
    @Transactional
    public void getContactInfo() throws Exception {
        // Initialize the database
        contactInfoRepository.saveAndFlush(contactInfo);

        // Get the contactInfo
        restContactInfoMockMvc.perform(get("/api/contactInfos/{id}", contactInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(contactInfo.getId().intValue()))
            .andExpect(jsonPath("$.infoType").value(DEFAULT_INFO_TYPE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingContactInfo() throws Exception {
        // Get the contactInfo
        restContactInfoMockMvc.perform(get("/api/contactInfos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactInfo() throws Exception {
        // Initialize the database
        contactInfoRepository.saveAndFlush(contactInfo);

		int databaseSizeBeforeUpdate = contactInfoRepository.findAll().size();

        // Update the contactInfo
        contactInfo.setInfoType(UPDATED_INFO_TYPE);
        contactInfo.setEmail(UPDATED_EMAIL);
        contactInfo.setPhone(UPDATED_PHONE);
        contactInfo.setWebsite(UPDATED_WEBSITE);

        restContactInfoMockMvc.perform(put("/api/contactInfos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contactInfo)))
                .andExpect(status().isOk());

        // Validate the ContactInfo in the database
        List<ContactInfo> contactInfos = contactInfoRepository.findAll();
        assertThat(contactInfos).hasSize(databaseSizeBeforeUpdate);
        ContactInfo testContactInfo = contactInfos.get(contactInfos.size() - 1);
        assertThat(testContactInfo.getInfoType()).isEqualTo(UPDATED_INFO_TYPE);
        assertThat(testContactInfo.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testContactInfo.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testContactInfo.getWebsite()).isEqualTo(UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void deleteContactInfo() throws Exception {
        // Initialize the database
        contactInfoRepository.saveAndFlush(contactInfo);

		int databaseSizeBeforeDelete = contactInfoRepository.findAll().size();

        // Get the contactInfo
        restContactInfoMockMvc.perform(delete("/api/contactInfos/{id}", contactInfo.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ContactInfo> contactInfos = contactInfoRepository.findAll();
        assertThat(contactInfos).hasSize(databaseSizeBeforeDelete - 1);
    }
}
