package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.Deals;
import com.karrmic.repository.DealsRepository;
import com.karrmic.repository.search.DealsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.karrmic.domain.enumeration.DealType;

/**
 * Test class for the DealsResource REST controller.
 *
 * @see DealsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DealsResourceTest {



private static final DealType DEFAULT_DEAL_TYPE = DealType.currentweek;
    private static final DealType UPDATED_DEAL_TYPE = DealType.nextweek;

    private static final Integer DEFAULT_WEIGHT = 1;
    private static final Integer UPDATED_WEIGHT = 2;

    @Inject
    private DealsRepository dealsRepository;

    @Inject
    private DealsSearchRepository dealsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDealsMockMvc;

    private Deals deals;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DealsResource dealsResource = new DealsResource();
        ReflectionTestUtils.setField(dealsResource, "dealsRepository", dealsRepository);
        ReflectionTestUtils.setField(dealsResource, "dealsSearchRepository", dealsSearchRepository);
        this.restDealsMockMvc = MockMvcBuilders.standaloneSetup(dealsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        deals = new Deals();
        deals.setDealType(DEFAULT_DEAL_TYPE);
        deals.setWeight(DEFAULT_WEIGHT);
    }

    @Test
    @Transactional
    public void createDeals() throws Exception {
        int databaseSizeBeforeCreate = dealsRepository.findAll().size();

        // Create the Deals

        restDealsMockMvc.perform(post("/api/dealss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(deals)))
                .andExpect(status().isCreated());

        // Validate the Deals in the database
        List<Deals> dealss = dealsRepository.findAll();
        assertThat(dealss).hasSize(databaseSizeBeforeCreate + 1);
        Deals testDeals = dealss.get(dealss.size() - 1);
        assertThat(testDeals.getDealType()).isEqualTo(DEFAULT_DEAL_TYPE);
        assertThat(testDeals.getWeight()).isEqualTo(DEFAULT_WEIGHT);
    }

    @Test
    @Transactional
    public void checkDealTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dealsRepository.findAll().size();
        // set the field null
        deals.setDealType(null);

        // Create the Deals, which fails.

        restDealsMockMvc.perform(post("/api/dealss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(deals)))
                .andExpect(status().isBadRequest());

        List<Deals> dealss = dealsRepository.findAll();
        assertThat(dealss).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDealss() throws Exception {
        // Initialize the database
        dealsRepository.saveAndFlush(deals);

        // Get all the dealss
        restDealsMockMvc.perform(get("/api/dealss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(deals.getId().intValue())))
                .andExpect(jsonPath("$.[*].dealType").value(hasItem(DEFAULT_DEAL_TYPE.toString())))
                .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT)));
    }

    @Test
    @Transactional
    public void getDeals() throws Exception {
        // Initialize the database
        dealsRepository.saveAndFlush(deals);

        // Get the deals
        restDealsMockMvc.perform(get("/api/dealss/{id}", deals.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(deals.getId().intValue()))
            .andExpect(jsonPath("$.dealType").value(DEFAULT_DEAL_TYPE.toString()))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT));
    }

    @Test
    @Transactional
    public void getNonExistingDeals() throws Exception {
        // Get the deals
        restDealsMockMvc.perform(get("/api/dealss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeals() throws Exception {
        // Initialize the database
        dealsRepository.saveAndFlush(deals);

		int databaseSizeBeforeUpdate = dealsRepository.findAll().size();

        // Update the deals
        deals.setDealType(UPDATED_DEAL_TYPE);
        deals.setWeight(UPDATED_WEIGHT);

        restDealsMockMvc.perform(put("/api/dealss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(deals)))
                .andExpect(status().isOk());

        // Validate the Deals in the database
        List<Deals> dealss = dealsRepository.findAll();
        assertThat(dealss).hasSize(databaseSizeBeforeUpdate);
        Deals testDeals = dealss.get(dealss.size() - 1);
        assertThat(testDeals.getDealType()).isEqualTo(UPDATED_DEAL_TYPE);
        assertThat(testDeals.getWeight()).isEqualTo(UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void deleteDeals() throws Exception {
        // Initialize the database
        dealsRepository.saveAndFlush(deals);

		int databaseSizeBeforeDelete = dealsRepository.findAll().size();

        // Get the deals
        restDealsMockMvc.perform(delete("/api/dealss/{id}", deals.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Deals> dealss = dealsRepository.findAll();
        assertThat(dealss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
