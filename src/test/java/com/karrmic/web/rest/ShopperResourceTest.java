package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.Shopper;
import com.karrmic.repository.ShopperRepository;
import com.karrmic.repository.search.ShopperSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ShopperResource REST controller.
 *
 * @see ShopperResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ShopperResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_FULLNAME = "AAA";
    private static final String UPDATED_FULLNAME = "BBB";
    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBB";

    private static final DateTime DEFAULT_REGISTER_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_REGISTER_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_REGISTER_DATE_STR = dateTimeFormatter.print(DEFAULT_REGISTER_DATE);

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final Integer DEFAULT_KARMA_POINTS = 1;
    private static final Integer UPDATED_KARMA_POINTS = 2;

    @Inject
    private ShopperRepository shopperRepository;

    @Inject
    private ShopperSearchRepository shopperSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restShopperMockMvc;

    private Shopper shopper;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ShopperResource shopperResource = new ShopperResource();
        ReflectionTestUtils.setField(shopperResource, "shopperRepository", shopperRepository);
        ReflectionTestUtils.setField(shopperResource, "shopperSearchRepository", shopperSearchRepository);
        this.restShopperMockMvc = MockMvcBuilders.standaloneSetup(shopperResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        shopper = new Shopper();
        shopper.setFullname(DEFAULT_FULLNAME);
        shopper.setEmailAddress(DEFAULT_EMAIL_ADDRESS);
        shopper.setRegisterDate(DEFAULT_REGISTER_DATE);
        shopper.setIsActive(DEFAULT_IS_ACTIVE);
        shopper.setKarmaPoints(DEFAULT_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void createShopper() throws Exception {
        int databaseSizeBeforeCreate = shopperRepository.findAll().size();

        // Create the Shopper

        restShopperMockMvc.perform(post("/api/shoppers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shopper)))
                .andExpect(status().isCreated());

        // Validate the Shopper in the database
        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeCreate + 1);
        Shopper testShopper = shoppers.get(shoppers.size() - 1);
        assertThat(testShopper.getFullname()).isEqualTo(DEFAULT_FULLNAME);
        assertThat(testShopper.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testShopper.getRegisterDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_REGISTER_DATE);
        assertThat(testShopper.getIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testShopper.getKarmaPoints()).isEqualTo(DEFAULT_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void checkFullnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopperRepository.findAll().size();
        // set the field null
        shopper.setFullname(null);

        // Create the Shopper, which fails.

        restShopperMockMvc.perform(post("/api/shoppers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shopper)))
                .andExpect(status().isBadRequest());

        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopperRepository.findAll().size();
        // set the field null
        shopper.setEmailAddress(null);

        // Create the Shopper, which fails.

        restShopperMockMvc.perform(post("/api/shoppers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shopper)))
                .andExpect(status().isBadRequest());

        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRegisterDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopperRepository.findAll().size();
        // set the field null
        shopper.setRegisterDate(null);

        // Create the Shopper, which fails.

        restShopperMockMvc.perform(post("/api/shoppers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shopper)))
                .andExpect(status().isBadRequest());

        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppers() throws Exception {
        // Initialize the database
        shopperRepository.saveAndFlush(shopper);

        // Get all the shoppers
        restShopperMockMvc.perform(get("/api/shoppers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(shopper.getId().intValue())))
                .andExpect(jsonPath("$.[*].fullname").value(hasItem(DEFAULT_FULLNAME.toString())))
                .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].registerDate").value(hasItem(DEFAULT_REGISTER_DATE_STR)))
                .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].karmaPoints").value(hasItem(DEFAULT_KARMA_POINTS)));
    }

    @Test
    @Transactional
    public void getShopper() throws Exception {
        // Initialize the database
        shopperRepository.saveAndFlush(shopper);

        // Get the shopper
        restShopperMockMvc.perform(get("/api/shoppers/{id}", shopper.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(shopper.getId().intValue()))
            .andExpect(jsonPath("$.fullname").value(DEFAULT_FULLNAME.toString()))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.registerDate").value(DEFAULT_REGISTER_DATE_STR))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.karmaPoints").value(DEFAULT_KARMA_POINTS));
    }

    @Test
    @Transactional
    public void getNonExistingShopper() throws Exception {
        // Get the shopper
        restShopperMockMvc.perform(get("/api/shoppers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShopper() throws Exception {
        // Initialize the database
        shopperRepository.saveAndFlush(shopper);

		int databaseSizeBeforeUpdate = shopperRepository.findAll().size();

        // Update the shopper
        shopper.setFullname(UPDATED_FULLNAME);
        shopper.setEmailAddress(UPDATED_EMAIL_ADDRESS);
        shopper.setRegisterDate(UPDATED_REGISTER_DATE);
        shopper.setIsActive(UPDATED_IS_ACTIVE);
        shopper.setKarmaPoints(UPDATED_KARMA_POINTS);

        restShopperMockMvc.perform(put("/api/shoppers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(shopper)))
                .andExpect(status().isOk());

        // Validate the Shopper in the database
        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeUpdate);
        Shopper testShopper = shoppers.get(shoppers.size() - 1);
        assertThat(testShopper.getFullname()).isEqualTo(UPDATED_FULLNAME);
        assertThat(testShopper.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testShopper.getRegisterDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_REGISTER_DATE);
        assertThat(testShopper.getIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testShopper.getKarmaPoints()).isEqualTo(UPDATED_KARMA_POINTS);
    }

    @Test
    @Transactional
    public void deleteShopper() throws Exception {
        // Initialize the database
        shopperRepository.saveAndFlush(shopper);

		int databaseSizeBeforeDelete = shopperRepository.findAll().size();

        // Get the shopper
        restShopperMockMvc.perform(delete("/api/shoppers/{id}", shopper.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Shopper> shoppers = shopperRepository.findAll();
        assertThat(shoppers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
