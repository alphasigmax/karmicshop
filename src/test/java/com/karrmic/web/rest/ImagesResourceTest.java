package com.karrmic.web.rest;

import com.karrmic.Application;
import com.karrmic.domain.Images;
import com.karrmic.repository.ImagesRepository;
import com.karrmic.repository.search.ImagesSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.karrmic.domain.enumeration.ImageType;
import com.karrmic.domain.enumeration.ImageCategory;

/**
 * Test class for the ImagesResource REST controller.
 *
 * @see ImagesResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ImagesResourceTest {



private static final ImageType DEFAULT_IMAGE_TYPE = ImageType.thumbnail;
    private static final ImageType UPDATED_IMAGE_TYPE = ImageType.smallpic;
    private static final String DEFAULT_IMAGE_PATH = "AAAAA";
    private static final String UPDATED_IMAGE_PATH = "BBBBB";


private static final ImageCategory DEFAULT_IMAGE_CATEGORY = ImageCategory.product;
    private static final ImageCategory UPDATED_IMAGE_CATEGORY = ImageCategory.seller;

    private static final Long DEFAULT_CATEGORY_ID = 1L;
    private static final Long UPDATED_CATEGORY_ID = 2L;

    @Inject
    private ImagesRepository imagesRepository;

    @Inject
    private ImagesSearchRepository imagesSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restImagesMockMvc;

    private Images images;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ImagesResource imagesResource = new ImagesResource();
        ReflectionTestUtils.setField(imagesResource, "imagesRepository", imagesRepository);
        ReflectionTestUtils.setField(imagesResource, "imagesSearchRepository", imagesSearchRepository);
        this.restImagesMockMvc = MockMvcBuilders.standaloneSetup(imagesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        images = new Images();
        images.setImageType(DEFAULT_IMAGE_TYPE);
        images.setImagePath(DEFAULT_IMAGE_PATH);
        images.setImageCategory(DEFAULT_IMAGE_CATEGORY);
        images.setCategoryId(DEFAULT_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void createImages() throws Exception {
        int databaseSizeBeforeCreate = imagesRepository.findAll().size();

        // Create the Images

        restImagesMockMvc.perform(post("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isCreated());

        // Validate the Images in the database
        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeCreate + 1);
        Images testImages = imagess.get(imagess.size() - 1);
        assertThat(testImages.getImageType()).isEqualTo(DEFAULT_IMAGE_TYPE);
        assertThat(testImages.getImagePath()).isEqualTo(DEFAULT_IMAGE_PATH);
        assertThat(testImages.getImageCategory()).isEqualTo(DEFAULT_IMAGE_CATEGORY);
        assertThat(testImages.getCategoryId()).isEqualTo(DEFAULT_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void checkImageTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = imagesRepository.findAll().size();
        // set the field null
        images.setImageType(null);

        // Create the Images, which fails.

        restImagesMockMvc.perform(post("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isBadRequest());

        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkImagePathIsRequired() throws Exception {
        int databaseSizeBeforeTest = imagesRepository.findAll().size();
        // set the field null
        images.setImagePath(null);

        // Create the Images, which fails.

        restImagesMockMvc.perform(post("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isBadRequest());

        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkImageCategoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = imagesRepository.findAll().size();
        // set the field null
        images.setImageCategory(null);

        // Create the Images, which fails.

        restImagesMockMvc.perform(post("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isBadRequest());

        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCategoryIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = imagesRepository.findAll().size();
        // set the field null
        images.setCategoryId(null);

        // Create the Images, which fails.

        restImagesMockMvc.perform(post("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isBadRequest());

        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllImagess() throws Exception {
        // Initialize the database
        imagesRepository.saveAndFlush(images);

        // Get all the imagess
        restImagesMockMvc.perform(get("/api/imagess"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(images.getId().intValue())))
                .andExpect(jsonPath("$.[*].imageType").value(hasItem(DEFAULT_IMAGE_TYPE.toString())))
                .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())))
                .andExpect(jsonPath("$.[*].imageCategory").value(hasItem(DEFAULT_IMAGE_CATEGORY.toString())))
                .andExpect(jsonPath("$.[*].categoryId").value(hasItem(DEFAULT_CATEGORY_ID.intValue())));
    }

    @Test
    @Transactional
    public void getImages() throws Exception {
        // Initialize the database
        imagesRepository.saveAndFlush(images);

        // Get the images
        restImagesMockMvc.perform(get("/api/imagess/{id}", images.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(images.getId().intValue()))
            .andExpect(jsonPath("$.imageType").value(DEFAULT_IMAGE_TYPE.toString()))
            .andExpect(jsonPath("$.imagePath").value(DEFAULT_IMAGE_PATH.toString()))
            .andExpect(jsonPath("$.imageCategory").value(DEFAULT_IMAGE_CATEGORY.toString()))
            .andExpect(jsonPath("$.categoryId").value(DEFAULT_CATEGORY_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingImages() throws Exception {
        // Get the images
        restImagesMockMvc.perform(get("/api/imagess/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImages() throws Exception {
        // Initialize the database
        imagesRepository.saveAndFlush(images);

		int databaseSizeBeforeUpdate = imagesRepository.findAll().size();

        // Update the images
        images.setImageType(UPDATED_IMAGE_TYPE);
        images.setImagePath(UPDATED_IMAGE_PATH);
        images.setImageCategory(UPDATED_IMAGE_CATEGORY);
        images.setCategoryId(UPDATED_CATEGORY_ID);

        restImagesMockMvc.perform(put("/api/imagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(images)))
                .andExpect(status().isOk());

        // Validate the Images in the database
        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeUpdate);
        Images testImages = imagess.get(imagess.size() - 1);
        assertThat(testImages.getImageType()).isEqualTo(UPDATED_IMAGE_TYPE);
        assertThat(testImages.getImagePath()).isEqualTo(UPDATED_IMAGE_PATH);
        assertThat(testImages.getImageCategory()).isEqualTo(UPDATED_IMAGE_CATEGORY);
        assertThat(testImages.getCategoryId()).isEqualTo(UPDATED_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void deleteImages() throws Exception {
        // Initialize the database
        imagesRepository.saveAndFlush(images);

		int databaseSizeBeforeDelete = imagesRepository.findAll().size();

        // Get the images
        restImagesMockMvc.perform(delete("/api/imagess/{id}", images.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Images> imagess = imagesRepository.findAll();
        assertThat(imagess).hasSize(databaseSizeBeforeDelete - 1);
    }
}
