 'use strict';

angular.module('karrmicApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-karrmicApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-karrmicApp-params')});
                }
                return response;
            }
        };
    });
