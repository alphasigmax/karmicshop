/* globals $ */
'use strict';

angular.module('karrmicApp')
    .directive('karrmicAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
