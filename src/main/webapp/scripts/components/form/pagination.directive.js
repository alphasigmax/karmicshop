/* globals $ */
'use strict';

angular.module('karrmicApp')
    .directive('karrmicAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
