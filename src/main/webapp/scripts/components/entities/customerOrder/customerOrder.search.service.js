'use strict';

angular.module('karrmicApp')
    .factory('CustomerOrderSearch', function ($resource) {
        return $resource('api/_search/customerOrders/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
