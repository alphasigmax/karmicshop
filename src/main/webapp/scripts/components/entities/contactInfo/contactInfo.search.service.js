'use strict';

angular.module('karrmicApp')
    .factory('ContactInfoSearch', function ($resource) {
        return $resource('api/_search/contactInfos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
