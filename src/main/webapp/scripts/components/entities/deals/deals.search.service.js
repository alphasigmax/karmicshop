'use strict';

angular.module('karrmicApp')
    .factory('DealsSearch', function ($resource) {
        return $resource('api/_search/dealss/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
