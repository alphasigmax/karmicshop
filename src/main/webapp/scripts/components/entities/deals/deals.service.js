'use strict';

angular.module('karrmicApp')
    .factory('Deals', function ($resource, DateUtils) {
        return $resource('api/dealss/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
