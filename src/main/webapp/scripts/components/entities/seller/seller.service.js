'use strict';

angular.module('karrmicApp')
    .factory('Seller', function ($resource, DateUtils) {
        return $resource('api/sellers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.registerDate = DateUtils.convertDateTimeFromServer(data.registerDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
