'use strict';

angular.module('karrmicApp')
    .factory('Images', function ($resource, DateUtils) {
        return $resource('api/imagess/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
