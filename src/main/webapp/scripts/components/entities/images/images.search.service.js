'use strict';

angular.module('karrmicApp')
    .factory('ImagesSearch', function ($resource) {
        return $resource('api/_search/imagess/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
