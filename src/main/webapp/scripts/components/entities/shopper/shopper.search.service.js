'use strict';

angular.module('karrmicApp')
    .factory('ShopperSearch', function ($resource) {
        return $resource('api/_search/shoppers/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
