'use strict';

angular.module('karrmicApp')
    .factory('Shopper', function ($resource, DateUtils) {
        return $resource('api/shoppers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.registerDate = DateUtils.convertDateTimeFromServer(data.registerDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
