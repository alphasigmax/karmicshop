'use strict';

angular.module('karrmicApp')
    .factory('Discount', function ($resource, DateUtils) {
        return $resource('api/discounts/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.expiryDate = DateUtils.convertDateTimeFromServer(data.expiryDate);
                    data.startDate = DateUtils.convertDateTimeFromServer(data.startDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
