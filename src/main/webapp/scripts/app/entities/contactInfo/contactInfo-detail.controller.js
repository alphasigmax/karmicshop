'use strict';

angular.module('karrmicApp')
    .controller('ContactInfoDetailController', function ($scope, $rootScope, $stateParams, entity, ContactInfo, Shopper, Seller) {
        $scope.contactInfo = entity;
        $scope.load = function (id) {
            ContactInfo.get({id: id}, function(result) {
                $scope.contactInfo = result;
            });
        };
        $rootScope.$on('karrmicApp:contactInfoUpdate', function(event, result) {
            $scope.contactInfo = result;
        });
    });
