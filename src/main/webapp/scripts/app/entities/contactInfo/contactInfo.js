'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('contactInfo', {
                parent: 'entity',
                url: '/contactInfos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.contactInfo.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/contactInfo/contactInfos.html',
                        controller: 'ContactInfoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('contactInfo');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('contactInfo.detail', {
                parent: 'entity',
                url: '/contactInfo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.contactInfo.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/contactInfo/contactInfo-detail.html',
                        controller: 'ContactInfoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('contactInfo');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ContactInfo', function($stateParams, ContactInfo) {
                        return ContactInfo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('contactInfo.new', {
                parent: 'contactInfo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/contactInfo/contactInfo-dialog.html',
                        controller: 'ContactInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    infoType: null,
                                    email: null,
                                    phone: null,
                                    website: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('contactInfo', null, { reload: true });
                    }, function() {
                        $state.go('contactInfo');
                    })
                }]
            })
            .state('contactInfo.edit', {
                parent: 'contactInfo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/contactInfo/contactInfo-dialog.html',
                        controller: 'ContactInfoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ContactInfo', function(ContactInfo) {
                                return ContactInfo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('contactInfo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
