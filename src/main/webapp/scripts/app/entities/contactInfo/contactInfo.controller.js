'use strict';

angular.module('karrmicApp')
    .controller('ContactInfoController', function ($scope, ContactInfo, ContactInfoSearch, ParseLinks) {
        $scope.contactInfos = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ContactInfo.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.contactInfos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ContactInfo.get({id: id}, function(result) {
                $scope.contactInfo = result;
                $('#deleteContactInfoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ContactInfo.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteContactInfoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ContactInfoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.contactInfos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.contactInfo = {
                infoType: null,
                email: null,
                phone: null,
                website: null,
                id: null
            };
        };
    });
