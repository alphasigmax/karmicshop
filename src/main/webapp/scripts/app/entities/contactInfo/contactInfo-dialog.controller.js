'use strict';

angular.module('karrmicApp').controller('ContactInfoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ContactInfo', 'Shopper', 'Seller',
        function($scope, $stateParams, $modalInstance, entity, ContactInfo, Shopper, Seller) {

        $scope.contactInfo = entity;
        $scope.shoppers = Shopper.query();
        $scope.sellers = Seller.query();
        $scope.load = function(id) {
            ContactInfo.get({id : id}, function(result) {
                $scope.contactInfo = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:contactInfoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.contactInfo.id != null) {
                ContactInfo.update($scope.contactInfo, onSaveFinished);
            } else {
                ContactInfo.save($scope.contactInfo, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
