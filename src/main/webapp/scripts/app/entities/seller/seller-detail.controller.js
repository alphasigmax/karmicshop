'use strict';

angular.module('karrmicApp')
    .controller('SellerDetailController', function ($scope, $rootScope, $stateParams, entity, Seller, Product, Address, ContactInfo) {
        $scope.seller = entity;
        $scope.load = function (id) {
            Seller.get({id: id}, function(result) {
                $scope.seller = result;
            });
        };
        $rootScope.$on('karrmicApp:sellerUpdate', function(event, result) {
            $scope.seller = result;
        });
    });
