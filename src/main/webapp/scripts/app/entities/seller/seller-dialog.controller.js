'use strict';

angular.module('karrmicApp').controller('SellerDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'Seller', 'Product', 'Address', 'ContactInfo',
        function($scope, $stateParams, $modalInstance, $q, entity, Seller, Product, Address, ContactInfo) {

        $scope.seller = entity;
        $scope.products = Product.query();
        $scope.addresss = Address.query();
        $scope.contactinfos = ContactInfo.query({filter: 'seller-is-null'});
        $q.all([$scope.seller.$promise, $scope.contactinfos.$promise]).then(function() {
            if (!$scope.seller.contactInfo.id) {
                return $q.reject();
            }
            return ContactInfo.get({id : $scope.seller.contactInfo.id}).$promise;
        }).then(function(contactInfo) {
            $scope.contactinfos.push(contactInfo);
        });
        $scope.load = function(id) {
            Seller.get({id : id}, function(result) {
                $scope.seller = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:sellerUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.seller.id != null) {
                Seller.update($scope.seller, onSaveFinished);
            } else {
                Seller.save($scope.seller, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
