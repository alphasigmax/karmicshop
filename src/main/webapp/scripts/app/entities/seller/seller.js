'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('seller', {
                parent: 'entity',
                url: '/sellers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.seller.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/seller/sellers.html',
                        controller: 'SellerController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('seller');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('seller.detail', {
                parent: 'entity',
                url: '/seller/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.seller.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/seller/seller-detail.html',
                        controller: 'SellerDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('seller');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Seller', function($stateParams, Seller) {
                        return Seller.get({id : $stateParams.id});
                    }]
                }
            })
            .state('seller.new', {
                parent: 'seller',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/seller/seller-dialog.html',
                        controller: 'SellerDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    fullName: null,
                                    emailAddress: null,
                                    registerDate: null,
                                    isActive: null,
                                    sellerInfo: null,
                                    karmaPoints: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('seller', null, { reload: true });
                    }, function() {
                        $state.go('seller');
                    })
                }]
            })
            .state('seller.edit', {
                parent: 'seller',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/seller/seller-dialog.html',
                        controller: 'SellerDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Seller', function(Seller) {
                                return Seller.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('seller', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
