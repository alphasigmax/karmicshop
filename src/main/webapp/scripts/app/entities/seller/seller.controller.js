'use strict';

angular.module('karrmicApp')
    .controller('SellerController', function ($scope, Seller, SellerSearch, ParseLinks) {
        $scope.sellers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Seller.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.sellers = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Seller.get({id: id}, function(result) {
                $scope.seller = result;
                $('#deleteSellerConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Seller.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSellerConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SellerSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.sellers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.seller = {
                fullName: null,
                emailAddress: null,
                registerDate: null,
                isActive: null,
                sellerInfo: null,
                karmaPoints: null,
                id: null
            };
        };
    });
