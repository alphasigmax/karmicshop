'use strict';

angular.module('karrmicApp')
    .controller('AddressDetailController', function ($scope, $rootScope, $stateParams, entity, Address, City) {
        $scope.address = entity;
        $scope.load = function (id) {
            Address.get({id: id}, function(result) {
                $scope.address = result;
            });
        };
        $rootScope.$on('karrmicApp:addressUpdate', function(event, result) {
            $scope.address = result;
        });
    });
