'use strict';

angular.module('karrmicApp')
    .controller('ReviewController', function ($scope, Review, ReviewSearch, ParseLinks) {
        $scope.reviews = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Review.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.reviews = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Review.get({id: id}, function(result) {
                $scope.review = result;
                $('#deleteReviewConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Review.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteReviewConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ReviewSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.reviews = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.review = {
                title: null,
                reviewDetails: null,
                rating: null,
                dateCreated: null,
                id: null
            };
        };
    });
