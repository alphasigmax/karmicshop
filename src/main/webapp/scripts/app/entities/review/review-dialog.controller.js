'use strict';

angular.module('karrmicApp').controller('ReviewDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Review', 'Shopper', 'Product',
        function($scope, $stateParams, $modalInstance, entity, Review, Shopper, Product) {

        $scope.review = entity;
        $scope.shoppers = Shopper.query();
        $scope.products = Product.query();
        $scope.load = function(id) {
            Review.get({id : id}, function(result) {
                $scope.review = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:reviewUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.review.id != null) {
                Review.update($scope.review, onSaveFinished);
            } else {
                Review.save($scope.review, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
