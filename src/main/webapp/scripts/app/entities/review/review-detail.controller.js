'use strict';

angular.module('karrmicApp')
    .controller('ReviewDetailController', function ($scope, $rootScope, $stateParams, entity, Review, Shopper, Product) {
        $scope.review = entity;
        $scope.load = function (id) {
            Review.get({id: id}, function(result) {
                $scope.review = result;
            });
        };
        $rootScope.$on('karrmicApp:reviewUpdate', function(event, result) {
            $scope.review = result;
        });
    });
