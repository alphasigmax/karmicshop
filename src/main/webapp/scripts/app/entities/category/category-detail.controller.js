'use strict';

angular.module('karrmicApp')
    .controller('CategoryDetailController', function ($scope, $rootScope, $stateParams, entity, Category, Images) {
        $scope.category = entity;
        $scope.load = function (id) {
            Category.get({id: id}, function(result) {
                $scope.category = result;
            });
        };
        $rootScope.$on('karrmicApp:categoryUpdate', function(event, result) {
            $scope.category = result;
        });
    });
