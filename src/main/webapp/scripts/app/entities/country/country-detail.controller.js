'use strict';

angular.module('karrmicApp')
    .controller('CountryDetailController', function ($scope, $rootScope, $stateParams, entity, Country) {
        $scope.country = entity;
        $scope.load = function (id) {
            Country.get({id: id}, function(result) {
                $scope.country = result;
            });
        };
        $rootScope.$on('karrmicApp:countryUpdate', function(event, result) {
            $scope.country = result;
        });
    });
