'use strict';

angular.module('karrmicApp').controller('CustomerOrderDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'CustomerOrder',
        function($scope, $stateParams, $modalInstance, entity, CustomerOrder) {

        $scope.customerOrder = entity;
        $scope.load = function(id) {
            CustomerOrder.get({id : id}, function(result) {
                $scope.customerOrder = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:customerOrderUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.customerOrder.id != null) {
                CustomerOrder.update($scope.customerOrder, onSaveFinished);
            } else {
                CustomerOrder.save($scope.customerOrder, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
