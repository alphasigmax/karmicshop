'use strict';

angular.module('karrmicApp')
    .controller('CustomerOrderDetailController', function ($scope, $rootScope, $stateParams, entity, CustomerOrder) {
        $scope.customerOrder = entity;
        $scope.load = function (id) {
            CustomerOrder.get({id: id}, function(result) {
                $scope.customerOrder = result;
            });
        };
        $rootScope.$on('karrmicApp:customerOrderUpdate', function(event, result) {
            $scope.customerOrder = result;
        });
    });
