'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('customerOrder', {
                parent: 'entity',
                url: '/customerOrders',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.customerOrder.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/customerOrder/customerOrders.html',
                        controller: 'CustomerOrderController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('customerOrder');
                        $translatePartialLoader.addPart('orderStatus');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('customerOrder.detail', {
                parent: 'entity',
                url: '/customerOrder/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.customerOrder.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/customerOrder/customerOrder-detail.html',
                        controller: 'CustomerOrderDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('customerOrder');
                        $translatePartialLoader.addPart('orderStatus');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'CustomerOrder', function($stateParams, CustomerOrder) {
                        return CustomerOrder.get({id : $stateParams.id});
                    }]
                }
            })
            .state('customerOrder.new', {
                parent: 'customerOrder',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/customerOrder/customerOrder-dialog.html',
                        controller: 'CustomerOrderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    amountPaid: null,
                                    dateCreated: null,
                                    transactionNumber: null,
                                    orderStatus: null,
                                    productId: null,
                                    quantity: null,
                                    shopperUserId: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('customerOrder', null, { reload: true });
                    }, function() {
                        $state.go('customerOrder');
                    })
                }]
            })
            .state('customerOrder.edit', {
                parent: 'customerOrder',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/customerOrder/customerOrder-dialog.html',
                        controller: 'CustomerOrderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CustomerOrder', function(CustomerOrder) {
                                return CustomerOrder.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('customerOrder', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
