'use strict';

angular.module('karrmicApp')
    .controller('CustomerOrderController', function ($scope, CustomerOrder, CustomerOrderSearch, ParseLinks) {
        $scope.customerOrders = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            CustomerOrder.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.customerOrders = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            CustomerOrder.get({id: id}, function(result) {
                $scope.customerOrder = result;
                $('#deleteCustomerOrderConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            CustomerOrder.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCustomerOrderConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CustomerOrderSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.customerOrders = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.customerOrder = {
                amountPaid: null,
                dateCreated: null,
                transactionNumber: null,
                orderStatus: null,
                productId: null,
                quantity: null,
                shopperUserId: null,
                id: null
            };
        };
    });
