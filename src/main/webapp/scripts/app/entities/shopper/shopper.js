'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('shopper', {
                parent: 'entity',
                url: '/shoppers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.shopper.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/shopper/shoppers.html',
                        controller: 'ShopperController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('shopper');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('shopper.detail', {
                parent: 'entity',
                url: '/shopper/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.shopper.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/shopper/shopper-detail.html',
                        controller: 'ShopperDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('shopper');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Shopper', function($stateParams, Shopper) {
                        return Shopper.get({id : $stateParams.id});
                    }]
                }
            })
            .state('shopper.new', {
                parent: 'shopper',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/shopper/shopper-dialog.html',
                        controller: 'ShopperDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    fullname: null,
                                    emailAddress: null,
                                    registerDate: null,
                                    isActive: null,
                                    karmaPoints: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('shopper', null, { reload: true });
                    }, function() {
                        $state.go('shopper');
                    })
                }]
            })
            .state('shopper.edit', {
                parent: 'shopper',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/shopper/shopper-dialog.html',
                        controller: 'ShopperDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Shopper', function(Shopper) {
                                return Shopper.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('shopper', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
