'use strict';

angular.module('karrmicApp')
    .controller('ShopperDetailController', function ($scope, $rootScope, $stateParams, entity, Shopper, Review, Address, ContactInfo) {
        $scope.shopper = entity;
        $scope.load = function (id) {
            Shopper.get({id: id}, function(result) {
                $scope.shopper = result;
            });
        };
        $rootScope.$on('karrmicApp:shopperUpdate', function(event, result) {
            $scope.shopper = result;
        });
    });
