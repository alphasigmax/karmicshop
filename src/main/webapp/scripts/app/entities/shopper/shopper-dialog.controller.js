'use strict';

angular.module('karrmicApp').controller('ShopperDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'Shopper', 'Review', 'Address', 'ContactInfo',
        function($scope, $stateParams, $modalInstance, $q, entity, Shopper, Review, Address, ContactInfo) {

        $scope.shopper = entity;
        $scope.reviews = Review.query();
        $scope.addresss = Address.query();
        $scope.contactinfos = ContactInfo.query({filter: 'shopper-is-null'});
        $q.all([$scope.shopper.$promise, $scope.contactinfos.$promise]).then(function() {
            if (!$scope.shopper.contactInfo.id) {
                return $q.reject();
            }
            return ContactInfo.get({id : $scope.shopper.contactInfo.id}).$promise;
        }).then(function(contactInfo) {
            $scope.contactinfos.push(contactInfo);
        });
        $scope.load = function(id) {
            Shopper.get({id : id}, function(result) {
                $scope.shopper = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:shopperUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.shopper.id != null) {
                Shopper.update($scope.shopper, onSaveFinished);
            } else {
                Shopper.save($scope.shopper, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
