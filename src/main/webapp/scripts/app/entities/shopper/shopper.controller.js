'use strict';

angular.module('karrmicApp')
    .controller('ShopperController', function ($scope, Shopper, ShopperSearch, ParseLinks) {
        $scope.shoppers = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Shopper.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.shoppers = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Shopper.get({id: id}, function(result) {
                $scope.shopper = result;
                $('#deleteShopperConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Shopper.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteShopperConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ShopperSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.shoppers = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.shopper = {
                fullname: null,
                emailAddress: null,
                registerDate: null,
                isActive: null,
                karmaPoints: null,
                id: null
            };
        };
    });
