'use strict';

angular.module('karrmicApp').controller('ProductDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'Product', 'Review', 'Category', 'Pricing', 'Seller', 'Images',
        function($scope, $stateParams, $modalInstance, $q, entity, Product, Review, Category, Pricing, Seller, Images) {

        $scope.product = entity;
        $scope.reviews = Review.query();
        $scope.categorys = Category.query();
        $scope.pricings = Pricing.query({filter: 'product-is-null'});
        $q.all([$scope.product.$promise, $scope.pricings.$promise]).then(function() {
            if (!$scope.product.pricing.id) {
                return $q.reject();
            }
            return Pricing.get({id : $scope.product.pricing.id}).$promise;
        }).then(function(pricing) {
            $scope.pricings.push(pricing);
        });
        $scope.sellers = Seller.query();
        $scope.imagess = Images.query();
        $scope.load = function(id) {
            Product.get({id : id}, function(result) {
                $scope.product = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:productUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.product.id != null) {
                Product.update($scope.product, onSaveFinished);
            } else {
                Product.save($scope.product, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
