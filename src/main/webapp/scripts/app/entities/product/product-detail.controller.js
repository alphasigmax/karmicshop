'use strict';

angular.module('karrmicApp')
    .controller('ProductDetailController', function ($scope, $rootScope, $stateParams, entity, Product, Review, Category, Pricing, Seller, Images) {
        $scope.product = entity;
        $scope.load = function (id) {
            Product.get({id: id}, function(result) {
                $scope.product = result;
            });
        };
        $rootScope.$on('karrmicApp:productUpdate', function(event, result) {
            $scope.product = result;
        });
    });
