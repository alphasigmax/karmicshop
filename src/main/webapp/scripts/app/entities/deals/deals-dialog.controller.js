'use strict';

angular.module('karrmicApp').controller('DealsDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'Deals', 'Product',
        function($scope, $stateParams, $modalInstance, $q, entity, Deals, Product) {

        $scope.deals = entity;
        $scope.products = Product.query({filter: 'deals-is-null'});
        $q.all([$scope.deals.$promise, $scope.products.$promise]).then(function() {
            if (!$scope.deals.product.id) {
                return $q.reject();
            }
            return Product.get({id : $scope.deals.product.id}).$promise;
        }).then(function(product) {
            $scope.products.push(product);
        });
        $scope.load = function(id) {
            Deals.get({id : id}, function(result) {
                $scope.deals = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:dealsUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.deals.id != null) {
                Deals.update($scope.deals, onSaveFinished);
            } else {
                Deals.save($scope.deals, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
