'use strict';

angular.module('karrmicApp')
    .controller('DealsDetailController', function ($scope, $rootScope, $stateParams, entity, Deals, Product) {
        $scope.deals = entity;
        $scope.load = function (id) {
            Deals.get({id: id}, function(result) {
                $scope.deals = result;
            });
        };
        $rootScope.$on('karrmicApp:dealsUpdate', function(event, result) {
            $scope.deals = result;
        });
    });
