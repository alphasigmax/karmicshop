'use strict';

angular.module('karrmicApp')
    .controller('DealsController', function ($scope, Deals, DealsSearch, ParseLinks) {
        $scope.dealss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Deals.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.dealss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Deals.get({id: id}, function(result) {
                $scope.deals = result;
                $('#deleteDealsConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Deals.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDealsConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DealsSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.dealss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.deals = {
                dealType: null,
                weight: null,
                id: null
            };
        };
    });
