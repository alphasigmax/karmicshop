'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('deals', {
                parent: 'entity',
                url: '/dealss',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.deals.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/deals/dealss.html',
                        controller: 'DealsController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('deals');
                        $translatePartialLoader.addPart('dealType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('deals.detail', {
                parent: 'entity',
                url: '/deals/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.deals.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/deals/deals-detail.html',
                        controller: 'DealsDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('deals');
                        $translatePartialLoader.addPart('dealType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Deals', function($stateParams, Deals) {
                        return Deals.get({id : $stateParams.id});
                    }]
                }
            })
            .state('deals.new', {
                parent: 'deals',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/deals/deals-dialog.html',
                        controller: 'DealsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    dealType: null,
                                    weight: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('deals', null, { reload: true });
                    }, function() {
                        $state.go('deals');
                    })
                }]
            })
            .state('deals.edit', {
                parent: 'deals',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/deals/deals-dialog.html',
                        controller: 'DealsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Deals', function(Deals) {
                                return Deals.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('deals', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
