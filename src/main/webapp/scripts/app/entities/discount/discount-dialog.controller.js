'use strict';

angular.module('karrmicApp').controller('DiscountDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Discount', 'Pricing',
        function($scope, $stateParams, $modalInstance, entity, Discount, Pricing) {

        $scope.discount = entity;
        $scope.pricings = Pricing.query();
        $scope.load = function(id) {
            Discount.get({id : id}, function(result) {
                $scope.discount = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:discountUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.discount.id != null) {
                Discount.update($scope.discount, onSaveFinished);
            } else {
                Discount.save($scope.discount, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
