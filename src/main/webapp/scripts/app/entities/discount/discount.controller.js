'use strict';

angular.module('karrmicApp')
    .controller('DiscountController', function ($scope, Discount, DiscountSearch, ParseLinks) {
        $scope.discounts = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Discount.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.discounts = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Discount.get({id: id}, function(result) {
                $scope.discount = result;
                $('#deleteDiscountConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Discount.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDiscountConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DiscountSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.discounts = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.discount = {
                discountType: null,
                quantityRequired: null,
                expiryDate: null,
                startDate: null,
                discountPrice: null,
                id: null
            };
        };
    });
