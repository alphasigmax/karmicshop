'use strict';

angular.module('karrmicApp')
    .controller('DiscountDetailController', function ($scope, $rootScope, $stateParams, entity, Discount, Pricing) {
        $scope.discount = entity;
        $scope.load = function (id) {
            Discount.get({id: id}, function(result) {
                $scope.discount = result;
            });
        };
        $rootScope.$on('karrmicApp:discountUpdate', function(event, result) {
            $scope.discount = result;
        });
    });
