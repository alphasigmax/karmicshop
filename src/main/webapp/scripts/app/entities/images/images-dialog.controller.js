'use strict';

angular.module('karrmicApp').controller('ImagesDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Images',
        function($scope, $stateParams, $modalInstance, entity, Images) {

        $scope.images = entity;
        $scope.load = function(id) {
            Images.get({id : id}, function(result) {
                $scope.images = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:imagesUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.images.id != null) {
                Images.update($scope.images, onSaveFinished);
            } else {
                Images.save($scope.images, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
