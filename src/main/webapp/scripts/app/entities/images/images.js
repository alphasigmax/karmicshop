'use strict';

angular.module('karrmicApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('images', {
                parent: 'entity',
                url: '/imagess',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.images.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/images/imagess.html',
                        controller: 'ImagesController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('images');
                        $translatePartialLoader.addPart('imageType');
                        $translatePartialLoader.addPart('imageCategory');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('images.detail', {
                parent: 'entity',
                url: '/images/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'karrmicApp.images.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/images/images-detail.html',
                        controller: 'ImagesDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('images');
                        $translatePartialLoader.addPart('imageType');
                        $translatePartialLoader.addPart('imageCategory');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Images', function($stateParams, Images) {
                        return Images.get({id : $stateParams.id});
                    }]
                }
            })
            .state('images.new', {
                parent: 'images',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/images/images-dialog.html',
                        controller: 'ImagesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    imageType: null,
                                    imagePath: null,
                                    imageCategory: null,
                                    categoryId: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('images', null, { reload: true });
                    }, function() {
                        $state.go('images');
                    })
                }]
            })
            .state('images.edit', {
                parent: 'images',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/images/images-dialog.html',
                        controller: 'ImagesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Images', function(Images) {
                                return Images.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('images', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
