'use strict';

angular.module('karrmicApp')
    .controller('ImagesDetailController', function ($scope, $rootScope, $stateParams, entity, Images) {
        $scope.images = entity;
        $scope.load = function (id) {
            Images.get({id: id}, function(result) {
                $scope.images = result;
            });
        };
        $rootScope.$on('karrmicApp:imagesUpdate', function(event, result) {
            $scope.images = result;
        });
    });
