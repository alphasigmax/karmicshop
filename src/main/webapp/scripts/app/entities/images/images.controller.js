'use strict';

angular.module('karrmicApp')
    .controller('ImagesController', function ($scope, Images, ImagesSearch, ParseLinks) {
        $scope.imagess = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Images.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.imagess = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Images.get({id: id}, function(result) {
                $scope.images = result;
                $('#deleteImagesConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Images.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteImagesConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ImagesSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.imagess = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.images = {
                imageType: null,
                imagePath: null,
                imageCategory: null,
                categoryId: null,
                id: null
            };
        };
    });
