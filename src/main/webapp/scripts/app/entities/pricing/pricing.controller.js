'use strict';

angular.module('karrmicApp')
    .controller('PricingController', function ($scope, Pricing, PricingSearch, ParseLinks) {
        $scope.pricings = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Pricing.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pricings = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Pricing.get({id: id}, function(result) {
                $scope.pricing = result;
                $('#deletePricingConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Pricing.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePricingConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PricingSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pricings = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pricing = {
                originalPrice: null,
                currentPrice: null,
                pricingType: null,
                id: null
            };
        };
    });
