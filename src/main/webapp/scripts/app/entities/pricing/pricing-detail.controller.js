'use strict';

angular.module('karrmicApp')
    .controller('PricingDetailController', function ($scope, $rootScope, $stateParams, entity, Pricing, Discount) {
        $scope.pricing = entity;
        $scope.load = function (id) {
            Pricing.get({id: id}, function(result) {
                $scope.pricing = result;
            });
        };
        $rootScope.$on('karrmicApp:pricingUpdate', function(event, result) {
            $scope.pricing = result;
        });
    });
