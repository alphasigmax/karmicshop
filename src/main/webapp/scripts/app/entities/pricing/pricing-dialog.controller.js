'use strict';

angular.module('karrmicApp').controller('PricingDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Pricing', 'Discount',
        function($scope, $stateParams, $modalInstance, entity, Pricing, Discount) {

        $scope.pricing = entity;
        $scope.discounts = Discount.query();
        $scope.load = function(id) {
            Pricing.get({id : id}, function(result) {
                $scope.pricing = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('karrmicApp:pricingUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.pricing.id != null) {
                Pricing.update($scope.pricing, onSaveFinished);
            } else {
                Pricing.save($scope.pricing, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
