'use strict';

angular.module('karrmicApp')
    .controller('CityDetailController', function ($scope, $rootScope, $stateParams, entity, City) {
        $scope.city = entity;
        $scope.load = function (id) {
            City.get({id: id}, function(result) {
                $scope.city = result;
            });
        };
        $rootScope.$on('karrmicApp:cityUpdate', function(event, result) {
            $scope.city = result;
        });
    });
