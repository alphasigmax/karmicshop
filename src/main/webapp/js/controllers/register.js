'use strict';

// signup controller
app.controller('RegisterFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.authError = null;
    $scope.register = function() {
      $scope.authError = null;
        // Try to create {"login":"getintouchapp","password":"admin","email":"admin@admin.com","langKey":"en"}
        $http.post('/api/register', {login: $scope.user.email, email: $scope.user.email, password: $scope.user.password,firstName:$scope.user.firstname})
      .then(function(response) {
        if ( !response.data.user ) {
          $scope.authError = response;
        }else{
          $state.go('app.dashboard');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };
  }])
 ;
