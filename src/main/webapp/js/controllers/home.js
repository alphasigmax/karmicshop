app.controller('HomeCtrl', function($scope, $http, loginCheck) {


    var products;
    var loaded = 0;
    $http.get('data/products.json').then(function(res){

        products = res.data;
        loaded = 1;
    });

    var interval  = setInterval(function(){

        $scope.items = [];

        var last = 0;
        $scope.loadMore = function() {

            if(last < products.length){

                $scope.items.push(products[last]);
                last++;

            }

        };
        clearInterval(interval);
    },1000);

    $http.get('data/recommendations.json').then(function(res){

        $scope.recommendations = res.data;

    });

    $http.get('data/menues.json').then(function(res){

        $scope.menues = res.data;

    });

    $http.get('data/basic_slider.json').then(function(res){

        $scope.basic_slider = res.data;

    });
});

