'use strict';

/* Controllers */
// signin controller
app.controller('LoginFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.authError = null;
    $scope.login = function() {
        $scope.authError = null;
        // Try to login
        $http({
            method: 'POST',
            url: '/api/authentication',
            data: $.param({j_username: $scope.user.j_username, j_password: $scope.user.j_password,"remember-me":"true", submit:"Login"}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .then(function(response) {
//show popup box with spinner then go to home page
                $state.go('app.dashboard');

            }, function(x) {
                $scope.authError = 'Email or Password not right';
            });
    };
}])
;
