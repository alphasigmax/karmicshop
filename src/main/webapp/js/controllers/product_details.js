//var App = angular.module('App', []);

app.controller('Product_detailCtrl', function($scope, $http,AsyncCallService) {

//	AsyncCallService.getData("/api/product/1").then(function(productResponse){
//		$scope.product = productResponse;
//	});

  $http.get('data/basic_images.json').then(function(res){

    $scope.basic_images = res.data;

  });

  $http.get('data/related_product.json').then(function(res){

    $scope.related_product = res.data;

  });

  $http.get('data/basic.json').then(function(res){

    $scope.basic = res.data;

  });

    $http.get('data/basic_images_small.json').then(function(res){

      $scope.basic_images_small = res.data;

    });
    $scope.addProductToCart = function(productId){
    	AsyncCallService.getData("/api/shoppingCart/"+productId)
    							.then(function(productResponse){
    									if(productResponse.statusCode == 200){
    										alert("Product added to cart");
    									}
    									else{
    										alert("could not add the product to shopping cart")
    									}
    							});
    }



});
