app.controller('CategoryUploadCtrl', ['$scope', '$http', 'FileUploader', function ($scope, $http, FileUploader) {
	$scope.message = false;
    $scope.category = {};
    var images = [];
   
    //$scope.category = {};
    $http({
        method: 'GET',
        url: 'api/categorys'
    }).success(function (data) {
        $scope.categoryList = data;
       // console.log("got category data " + data)

    });
    
    

    
    //save category
    $scope.saveCategory = function () {
    	//process parent Category
    	if(angular.isUndefined($scope.category.parentCategory) == true){
    		var parentCat = null;
    	}else{
    		parentCat = {'id' : $scope.category.parentCategory};
    	}
    	

        var formData = {
            'imagess': images,
            'categoryName': $scope.category.categoryName,
            'categoryInfo': $scope.category.categoryInfo,
            'parentCategory' : parentCat
        }


        $http({
            method: 'POST',
            url: 'api/categorys',
            data: formData

        }).success(function (response) {
        	$scope.messageClass ="alert-success";
        	$scope.message = 'Category has been added.';
        	
        	//reset form data
        	images =[];
            $scope.category = {};
            $scope.uploader.queue = [];
            $scope.uploader.progress = 0;
        }).error(function(response){
        	$scope.messageClass ="alert-danger";
        	$scope.message = 'Network error'
        });
        
        
    }


    var uploader = $scope.uploader = new FileUploader({
        url: '/upload/uploadImage'
    });


    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        item.formData.push({"imagecategory":  "category"});
        item.formData.push({"catid":  "2"});
        console.log(item);
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        images.push({'id': response.id});
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');

    };

    console.log(uploader);
    console.info('uploader', uploader);
}]);

