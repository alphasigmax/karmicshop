app.controller('ShoppingcartCtrl', ['$scope', '$http',
  function ($scope, $http) {
    $http.get('data/shoppingcart.json').success(function(data) {
      $scope.shoppingcart = data;
    });
}]);