/**
 * Basic service method of get and post to use throughout the application
 */

angular.module('App').service('AsyncCallService', function($http) {
		this.getData = function(url){
							return $http.get(url)
								 .then(function(response) {
									return response.data;
							});
						}
		this.postData = function(url,jsonDataToPost){
							return $http.get(url)
								 .then(function(response) {
									 	return response.data;
							});
						}
});