module.exports = {
  angular:{
    src:[
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-animate/angular-animate.js',
 /* //'bower_components/angular-cookies/angular-cookies.js',
    //'bower_components/angular-resource/angular-resource.js',
    //'bower_components/angular-sanitize/angular-sanitize.js', */
      'bower_components/angular-touch/angular-touch.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
/* //'bower_components/ngstorage/ngStorage.js', */
      'bower_components/angular-ui-utils/ui-utils.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/oclazyload/dist/ocLazyLoad.js',
      'bower_components/angular-loading-bar/build/loading-bar.min.js',
      'bower_components/perfect-scrollbar/min/perfect-scrollbar.min.js',
      'bower_components/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js',
      'bower_components/angular-inview/angular-inview.js',

    'js/app.js',
    'js/app.config.js',
    'js/app.lazyload.js',
    'js/app.router.js',
    'js/app.main.js',
    'js/services/ui-load.js',
    'js/filters/moment-fromNow.js',
    'js/directives/nganimate.js',
    'js/directives/ui-jq.js',
    'js/directives/ui-module.js',
    'js/directives/ui-nav.js',
    'js/directives/ui-bootstrap.js',
    'js/directives/ui-chatwindow.js',
    'js/directives/ui-sectionbox.js',
    'js/controllers/bootstrap.js',
    'js/controllers/topbar.js',
    'js/controllers/chat.js',
    'bower_components/Chart.js/Chart.min.js'

/*   'app/js/*.js',
      'app/js/directives/*.js',
      'app/js/services/*.js',
      'app/js/filters/*.js',
      'app/js/jq/*.js',
      'app/js/controllers/bootstrap.js',
      'app/js/controllers/topbar.js'*/
    ],
    dest:'angular/js/app.src.js'
  }
}
