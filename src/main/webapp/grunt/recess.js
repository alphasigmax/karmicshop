module.exports = {
    angular: {
        files: {
            'angular/css/app.min.css': [

                'bower_components/bootstrap/dist/css/bootstrap.css',
                'bower_components/bootstrap/dist/css/bootstrap-theme.css',
                /*'bower_components/font-awesome/css/font-awesome.css',*/
                'bower_components/animate.css/animate.css',
                /*'app/css/*.css',*/
                'css/app.css',
                'css/material-icons.css'
            ]
        },
        options: {
            compress: true
        }
    }
}
