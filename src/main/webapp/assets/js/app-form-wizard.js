var App = (function () {
  'use strict';
  
  App.wizard = function( ){

    //Fuel UX
    $('.wizard-ux').wizard();

    $('.wizard-ux').on('changed.fu.wizard',function(){
      //Bootstrap Slider
      $('.bslider').slider();
    });
    
    $(".wizard-next").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('next');
      e.preventDefault();
    });
    
    $(".wizard-previous").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('previous');
      e.preventDefault();
    });

   //Select2
    $(".select2").select2({
      width: '100%'
    });
    
    //Select2 tags
    $(".tags").select2({tags: true, width: '100%'});


    $(".rate_slider").slider().on("slide",function(e){
      var id = $(this).attr('id');
      $("#rate_"+id).html(e.value + "%");
    });
    
  };

  App.textEditors = function( ){

    //Summernote
    $('#editor1').summernote({
      height: 300
    });
    
  };

  return App;
})(App || {});

$(document).ready(function(){
  var count = 1;
  $(".addCF").click(function(){
    count++;
    var content = '<tr valign="top"><td><div class="form-group"><div class="col-sm-5"><label class="control-label">Number of sale</label><input type="text" placeholder="Number of sale" class="form-control"></div><div class="col-sm-5"><label class="control-label">Price discount: <span id="rate_'+count+'">5%</span></label><p></p><input id="'+count+'" data-slider-min="0" data-slider-max="100" type="text" value="5" class="rate_slider bslider form-control"></div><div class="col-md-2"><a href="javascript:void(0);" class="remCF btn btn-danger btn-sm">Remove</a></div></div></td></tr>';
    $("#customFields").append(content);
    $('.bslider').slider();

  });
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});