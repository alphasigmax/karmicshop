package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.Shopper;
import com.karrmic.repository.ShopperRepository;
import com.karrmic.repository.search.ShopperSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Shopper.
 */
@RestController
@RequestMapping("/api")
public class ShopperResource {

    private final Logger log = LoggerFactory.getLogger(ShopperResource.class);

    @Inject
    private ShopperRepository shopperRepository;

    @Inject
    private ShopperSearchRepository shopperSearchRepository;

    /**
     * POST  /shoppers -> Create a new shopper.
     */
    @RequestMapping(value = "/shoppers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Shopper> createShopper(@Valid @RequestBody Shopper shopper) throws URISyntaxException {
        log.debug("REST request to save Shopper : {}", shopper);
        if (shopper.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new shopper cannot already have an ID").body(null);
        }
        Shopper result = shopperRepository.save(shopper);
        shopperSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/shoppers/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("shopper", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /shoppers -> Updates an existing shopper.
     */
    @RequestMapping(value = "/shoppers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Shopper> updateShopper(@Valid @RequestBody Shopper shopper) throws URISyntaxException {
        log.debug("REST request to update Shopper : {}", shopper);
        if (shopper.getId() == null) {
            return createShopper(shopper);
        }
        Shopper result = shopperRepository.save(shopper);
        shopperSearchRepository.save(shopper);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("shopper", shopper.getId().toString()))
                .body(result);
    }

    /**
     * GET  /shoppers -> get all the shoppers.
     */
    @RequestMapping(value = "/shoppers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Shopper>> getAllShoppers(Pageable pageable)
        throws URISyntaxException {
        Page<Shopper> page = shopperRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shoppers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /shoppers/:id -> get the "id" shopper.
     */
    @RequestMapping(value = "/shoppers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Shopper> getShopper(@PathVariable Long id) {
        log.debug("REST request to get Shopper : {}", id);
        return Optional.ofNullable(shopperRepository.findOneWithEagerRelationships(id))
            .map(shopper -> new ResponseEntity<>(
                shopper,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /shoppers/:id -> delete the "id" shopper.
     */
    @RequestMapping(value = "/shoppers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteShopper(@PathVariable Long id) {
        log.debug("REST request to delete Shopper : {}", id);
        shopperRepository.delete(id);
        shopperSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("shopper", id.toString())).build();
    }

    /**
     * SEARCH  /_search/shoppers/:query -> search for the shopper corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/shoppers/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Shopper> searchShoppers(@PathVariable String query) {
        return StreamSupport
            .stream(shopperSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
