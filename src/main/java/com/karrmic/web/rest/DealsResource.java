package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.Deals;
import com.karrmic.repository.DealsRepository;
import com.karrmic.repository.search.DealsSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Deals.
 */
@RestController
@RequestMapping("/api")
public class DealsResource {

    private final Logger log = LoggerFactory.getLogger(DealsResource.class);

    @Inject
    private DealsRepository dealsRepository;

    @Inject
    private DealsSearchRepository dealsSearchRepository;

    /**
     * POST  /dealss -> Create a new deals.
     */
    @RequestMapping(value = "/dealss",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deals> createDeals(@Valid @RequestBody Deals deals) throws URISyntaxException {
        log.debug("REST request to save Deals : {}", deals);
        if (deals.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new deals cannot already have an ID").body(null);
        }
        Deals result = dealsRepository.save(deals);
        dealsSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/dealss/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("deals", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /dealss -> Updates an existing deals.
     */
    @RequestMapping(value = "/dealss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deals> updateDeals(@Valid @RequestBody Deals deals) throws URISyntaxException {
        log.debug("REST request to update Deals : {}", deals);
        if (deals.getId() == null) {
            return createDeals(deals);
        }
        Deals result = dealsRepository.save(deals);
        dealsSearchRepository.save(deals);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("deals", deals.getId().toString()))
                .body(result);
    }

    /**
     * GET  /dealss -> get all the dealss.
     */
    @RequestMapping(value = "/dealss",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Deals>> getAllDealss(Pageable pageable)
        throws URISyntaxException {
        Page<Deals> page = dealsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dealss");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dealss/:id -> get the "id" deals.
     */
    @RequestMapping(value = "/dealss/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Deals> getDeals(@PathVariable Long id) {
        log.debug("REST request to get Deals : {}", id);
        return Optional.ofNullable(dealsRepository.findOne(id))
            .map(deals -> new ResponseEntity<>(
                deals,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dealss/:id -> delete the "id" deals.
     */
    @RequestMapping(value = "/dealss/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDeals(@PathVariable Long id) {
        log.debug("REST request to delete Deals : {}", id);
        dealsRepository.delete(id);
        dealsSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("deals", id.toString())).build();
    }

    /**
     * SEARCH  /_search/dealss/:query -> search for the deals corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/dealss/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Deals> searchDealss(@PathVariable String query) {
        return StreamSupport
            .stream(dealsSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
