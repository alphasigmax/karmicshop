package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.CustomerOrder;
import com.karrmic.domain.Product;
import com.karrmic.domain.enumeration.OrderStatus;
import com.karrmic.repository.CustomerOrderRepository;
import com.karrmic.repository.ProductRepository;
import com.karrmic.repository.search.CustomerOrderSearchRepository;
import com.karrmic.service.UserService;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CustomerOrder.
 */
@RestController
@RequestMapping("/api")
public class CustomerOrderResource {

    private final Logger log = LoggerFactory.getLogger(CustomerOrderResource.class);

    @Inject
    private CustomerOrderRepository customerOrderRepository;

    @Inject
    private CustomerOrderSearchRepository customerOrderSearchRepository;

    @Inject
    private ProductRepository productRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /customerOrders -> Create a new customerOrder.
     */
    @RequestMapping(value = "/customerOrders",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> createCustomerOrder(@Valid @RequestBody CustomerOrder customerOrder, Principal principal) throws URISyntaxException {
        log.debug("REST request to save CustomerOrder : {}", customerOrder);
        if (customerOrder.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new customerOrder cannot already have an ID").body(null);
        }
        CustomerOrder result = customerOrderRepository.save(customerOrder);
        customerOrderSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/customerOrders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("customerOrder", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /customerOrders -> Updates an existing customerOrder.
     */
    @RequestMapping(value = "/customerOrders",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> updateCustomerOrder(@Valid @RequestBody CustomerOrder customerOrder, Principal principal) throws URISyntaxException {
        log.debug("REST request to update CustomerOrder : {}", customerOrder);
        if (customerOrder.getId() == null) {
            return createCustomerOrder(customerOrder, principal);
        }
        //get the shopper id and update the order with his ID

        CustomerOrder result = customerOrderRepository.save(customerOrder);
        customerOrderSearchRepository.save(customerOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("customerOrder", customerOrder.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customerOrders -> get all the customerOrders.
     * this should not be required. or modify so it only returns for particular customer
     */
    @RequestMapping(value = "/customerOrders",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CustomerOrder>> getAllCustomerOrders(Pageable pageable, Principal principal)
        throws URISyntaxException {
        Page<CustomerOrder> page = customerOrderRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customerOrders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * Get only items that are in shopping cart
     *
     * @param pageable
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = "/shoppingCart",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CustomerOrder>> getShoppingCart(Pageable pageable, Principal principal)
        throws URISyntaxException {
        final String currentUser = principal.getName();
        log.debug("Current User : {}", currentUser);
        Page<CustomerOrder> page = customerOrderRepository.findShoppingCartItems(pageable, currentUser);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/getShoppingCart");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * save the shopping cart
     *
     * @param
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = "/shoppingCart",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> saveShoppingCart(@Valid @RequestBody CustomerOrder customerOrder, Principal principal)
        throws URISyntaxException {
        final String currentUser = principal.getName();
        log.debug("Current User : {}", currentUser);
        customerOrder.setShopperUserId(currentUser);
        customerOrder.setOrderStatus(OrderStatus.INCART);
        CustomerOrder result = customerOrderRepository.save(customerOrder);
        customerOrderSearchRepository.save(customerOrder);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("customerOrder", customerOrder.getId().toString()))
            .body(result);
    }


    private CustomerOrder updateShoppingCart(String currentUser, Long productId, int quantity) {

        Product product = productRepository.findOne(productId);
        log.debug("Found product : " + product);
    //    if (null == product) return null;
        List<CustomerOrder> shoppingCarts = customerOrderRepository.findAllShoppingCart(currentUser, productId);
        log.debug("shopping carts for User : {}", shoppingCarts);
        CustomerOrder shoppingCart;
        //if null create and save it in db
        if (null == shoppingCarts || shoppingCarts.isEmpty()) {
            shoppingCart = new CustomerOrder();

        } else {
            shoppingCart = shoppingCarts.get(0);
        }
        shoppingCart.setShopperUserId(currentUser);
        shoppingCart.setProductId(productId);
//fetch the product from product repo

     //   int existingQuantity = shoppingCart.getQuantity();

        shoppingCart.setQuantity( quantity);
        return customerOrderRepository.save(shoppingCart);
        //return CustomerOrder (which will have all products that have been added so far)

    }

    /**
     * save the shopping cart
     *
     * @param
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = "/shoppingCart/{productId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> saveShoppingCart(@PathVariable Long productId, Principal principal)
        throws URISyntaxException {
        final String currentUser = principal.getName();
        log.debug("Current User : {}", currentUser);
        CustomerOrder result = updateShoppingCart(currentUser, productId, 1);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("customerOrder", result.getId().toString()))
            .body(result);
    }

    /**
     * save the shopping cart
     *
     * @param
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = "/shoppingCart/{productId}/{quantity}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> saveShoppingCart(@PathVariable Long productId, @PathVariable int quantity, Principal principal)
        throws URISyntaxException {
        final String currentUser = principal.getName();
        log.debug("Current User : {}", currentUser);
        CustomerOrder result = null;
        if (quantity > 0) {
            result = updateShoppingCart(currentUser, productId, quantity);
        } else {
            //remove the product from shopping cart here
        }
        if (null == result) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("customerOrder", result.getId().toString()))
                .body(result);
        }
    }


    /**
     * GET  /customerOrders/:id -> get the "id" customerOrder.
     */
    @RequestMapping(value = "/customerOrders/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomerOrder> getCustomerOrder(@PathVariable Long id) {
        log.debug("REST request to get CustomerOrder : {}", id);
        return Optional.ofNullable(customerOrderRepository.findOne(id))
            .map(customerOrder -> new ResponseEntity<>(
                customerOrder,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /customerOrders/:id -> delete the "id" customerOrder.
     */
    @RequestMapping(value = "/customerOrders/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCustomerOrder(@PathVariable Long id) {
        log.debug("REST request to delete CustomerOrder : {}", id);
        customerOrderRepository.delete(id);
        customerOrderSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("customerOrder", id.toString())).build();
    }

    /**
     * SEARCH  /_search/customerOrders/:query -> search for the customerOrder corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/customerOrders/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CustomerOrder> searchCustomerOrders(@PathVariable String query) {
        return StreamSupport
            .stream(customerOrderSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
