package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.ContactInfo;
import com.karrmic.repository.ContactInfoRepository;
import com.karrmic.repository.search.ContactInfoSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ContactInfo.
 */
@RestController
@RequestMapping("/api")
public class ContactInfoResource {

    private final Logger log = LoggerFactory.getLogger(ContactInfoResource.class);

    @Inject
    private ContactInfoRepository contactInfoRepository;

    @Inject
    private ContactInfoSearchRepository contactInfoSearchRepository;

    /**
     * POST  /contactInfos -> Create a new contactInfo.
     */
    @RequestMapping(value = "/contactInfos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactInfo> createContactInfo(@Valid @RequestBody ContactInfo contactInfo) throws URISyntaxException {
        log.debug("REST request to save ContactInfo : {}", contactInfo);
        if (contactInfo.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new contactInfo cannot already have an ID").body(null);
        }
        ContactInfo result = contactInfoRepository.save(contactInfo);
        contactInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/contactInfos/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("contactInfo", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /contactInfos -> Updates an existing contactInfo.
     */
    @RequestMapping(value = "/contactInfos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactInfo> updateContactInfo(@Valid @RequestBody ContactInfo contactInfo) throws URISyntaxException {
        log.debug("REST request to update ContactInfo : {}", contactInfo);
        if (contactInfo.getId() == null) {
            return createContactInfo(contactInfo);
        }
        ContactInfo result = contactInfoRepository.save(contactInfo);
        contactInfoSearchRepository.save(contactInfo);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("contactInfo", contactInfo.getId().toString()))
                .body(result);
    }

    /**
     * GET  /contactInfos -> get all the contactInfos.
     */
    @RequestMapping(value = "/contactInfos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ContactInfo>> getAllContactInfos(Pageable pageable, @RequestParam(required = false) String filter)
        throws URISyntaxException {
        if ("shopper-is-null".equals(filter)) {
            log.debug("REST request to get all ContactInfos where shopper is null");
            return new ResponseEntity<>(StreamSupport
                .stream(contactInfoRepository.findAll().spliterator(), false)
                .filter(contactInfo -> contactInfo.getShopper() == null)
                .collect(Collectors.toList()), HttpStatus.OK);
        }
        
        if ("seller-is-null".equals(filter)) {
            log.debug("REST request to get all ContactInfos where seller is null");
            return new ResponseEntity<>(StreamSupport
                .stream(contactInfoRepository.findAll().spliterator(), false)
                .filter(contactInfo -> contactInfo.getSeller() == null)
                .collect(Collectors.toList()), HttpStatus.OK);
        }
        
        Page<ContactInfo> page = contactInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contactInfos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /contactInfos/:id -> get the "id" contactInfo.
     */
    @RequestMapping(value = "/contactInfos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ContactInfo> getContactInfo(@PathVariable Long id) {
        log.debug("REST request to get ContactInfo : {}", id);
        return Optional.ofNullable(contactInfoRepository.findOne(id))
            .map(contactInfo -> new ResponseEntity<>(
                contactInfo,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /contactInfos/:id -> delete the "id" contactInfo.
     */
    @RequestMapping(value = "/contactInfos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteContactInfo(@PathVariable Long id) {
        log.debug("REST request to delete ContactInfo : {}", id);
        contactInfoRepository.delete(id);
        contactInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("contactInfo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/contactInfos/:query -> search for the contactInfo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/contactInfos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ContactInfo> searchContactInfos(@PathVariable String query) {
        return StreamSupport
            .stream(contactInfoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
