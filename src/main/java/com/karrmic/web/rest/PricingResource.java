package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.Discount;
import com.karrmic.domain.Pricing;
import com.karrmic.repository.PricingRepository;
import com.karrmic.repository.search.PricingSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pricing.
 */
@RestController
@RequestMapping("/api")
public class PricingResource {

    private final Logger log = LoggerFactory.getLogger(PricingResource.class);

    @Inject
    private PricingRepository pricingRepository;

    @Inject
    private PricingSearchRepository pricingSearchRepository;

    /**
     * POST  /pricings -> Create a new pricing.
     */
    @RequestMapping(value = "/pricings",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Pricing> createPricing(@Valid @RequestBody Pricing pricing) throws URISyntaxException {
        log.debug("REST request to save Pricing : {}", pricing);
        if (pricing.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pricing cannot already have an ID").body(null);
        }
        Set<Discount> discounts = pricing.getDiscounts();
        for(Discount discount: discounts)
        {
          discount.setPricing(pricing);
        }
        Pricing result = pricingRepository.save(pricing);
        pricingSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pricings/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("pricing", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /pricings -> Updates an existing pricing.
     */
    @RequestMapping(value = "/pricings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Pricing> updatePricing(@Valid @RequestBody Pricing pricing) throws URISyntaxException {
        log.debug("REST request to update Pricing : {}", pricing);
        if (pricing.getId() == null) {
            return createPricing(pricing);
        }
        Pricing result = pricingRepository.save(pricing);
        pricingSearchRepository.save(pricing);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("pricing", pricing.getId().toString()))
                .body(result);
    }

    /**
     * GET  /pricings -> get all the pricings.
     */
    @RequestMapping(value = "/pricings",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Pricing>> getAllPricings(Pageable pageable)
        throws URISyntaxException {
        Page<Pricing> page = pricingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pricings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pricings/:id -> get the "id" pricing.
     */
    @RequestMapping(value = "/pricings/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Pricing> getPricing(@PathVariable Long id) {
        log.debug("REST request to get Pricing : {}", id);
        return Optional.ofNullable(pricingRepository.findOne(id))
            .map(pricing -> new ResponseEntity<>(
                pricing,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pricings/:id -> delete the "id" pricing.
     */
    @RequestMapping(value = "/pricings/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePricing(@PathVariable Long id) {
        log.debug("REST request to delete Pricing : {}", id);
        pricingRepository.delete(id);
        pricingSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pricing", id.toString())).build();
    }

    /**
     * SEARCH  /_search/pricings/:query -> search for the pricing corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/pricings/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Pricing> searchPricings(@PathVariable String query) {
        return StreamSupport
            .stream(pricingSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
