/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.karrmic.web.rest.dto;
