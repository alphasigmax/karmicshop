package com.karrmic.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.karrmic.domain.Seller;
import com.karrmic.repository.SellerRepository;
import com.karrmic.repository.search.SellerSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import com.karrmic.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Seller.
 */
@RestController
@RequestMapping("/api")
public class SellerResource {

    private final Logger log = LoggerFactory.getLogger(SellerResource.class);

    @Inject
    private SellerRepository sellerRepository;

    @Inject
    private SellerSearchRepository sellerSearchRepository;

    /**
     * POST  /sellers -> Create a new seller.
     */
    @RequestMapping(value = "/sellers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seller> createSeller(@Valid @RequestBody Seller seller) throws URISyntaxException {
        log.debug("REST request to save Seller : {}", seller);
        if (seller.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new seller cannot already have an ID").body(null);
        }
        Seller result = sellerRepository.save(seller);
        sellerSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/sellers/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("seller", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /sellers -> Updates an existing seller.
     */
    @RequestMapping(value = "/sellers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seller> updateSeller(@Valid @RequestBody Seller seller) throws URISyntaxException {
        log.debug("REST request to update Seller : {}", seller);
        if (seller.getId() == null) {
            return createSeller(seller);
        }
        Seller result = sellerRepository.save(seller);
        sellerSearchRepository.save(seller);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("seller", seller.getId().toString()))
                .body(result);
    }

    /**
     * GET  /sellers -> get all the sellers.
     */
    @RequestMapping(value = "/sellers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Seller>> getAllSellers(Pageable pageable)
        throws URISyntaxException {
        Page<Seller> page = sellerRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sellers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sellers/:id -> get the "id" seller.
     */
    @RequestMapping(value = "/sellers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Seller> getSeller(@PathVariable Long id) {
        log.debug("REST request to get Seller : {}", id);
        return Optional.ofNullable(sellerRepository.findOneWithEagerRelationships(id))
            .map(seller -> new ResponseEntity<>(
                seller,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sellers/:id -> delete the "id" seller.
     */
    @RequestMapping(value = "/sellers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSeller(@PathVariable Long id) {
        log.debug("REST request to delete Seller : {}", id);
        sellerRepository.delete(id);
        sellerSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("seller", id.toString())).build();
    }

    /**
     * SEARCH  /_search/sellers/:query -> search for the seller corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/sellers/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Seller> searchSellers(@PathVariable String query) {
        return StreamSupport
            .stream(sellerSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
