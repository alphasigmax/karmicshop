package com.karrmic.service.seller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karrmic.domain.Seller;
import com.karrmic.repository.SellerRepository;

/**
 * This is seller service implementation class.
 * 
 * @author VishalZanzrukia
 *
 */
@Service
public class SellerServiceImpl implements SellerService {
	
	@Autowired
	private SellerRepository sellerRepository;
	
	@Override
	public void save(Seller seller){
		sellerRepository.saveAndFlush(seller);
	}

	@Override
	public List<Seller> getAllSellers() {
		return sellerRepository.findAll();
	}

	@Override
	public Seller findSellerById(Long id) {
		return sellerRepository.findOne(id);
	}
}
