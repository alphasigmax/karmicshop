package com.karrmic.service.seller;

import java.util.List;

import com.karrmic.domain.Seller;

/**
 * This is service which defines all {@link Seller} related operations.
 * 
 * @author VishalZanzrukia
 *
 */
public interface SellerService {
	
	/**
	 * save seller
	 * 
	 * @param {@link Seller}
	 */
	void save(Seller seller);
	
	/**
	 * get all sellers
	 * 
	 * @return list of {@link Seller}
	 */
	List<Seller> getAllSellers();
	
	/**
	 * @param id
	 * @return
	 */
	Seller findSellerById(Long id);

}
