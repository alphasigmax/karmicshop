package com.karrmic.service.paymentgateway;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.karrmic.constant.paymentgateway.PaymentConstants;
import com.karrmic.domain.Seller;
import com.karrmic.domain.paymentgateway.CreditCardPayment;
import com.karrmic.service.seller.SellerService;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Account;
import com.stripe.model.Charge;
import com.stripe.net.RequestOptions;

/**
 * @author VishalZanzrukia
 *
 */
@Service
public class StripeService {

	private static final Logger LOG = LoggerFactory.getLogger(StripeService.class);
	
	@Autowired
	private SellerService sellerService;

	/**
	 * process payment through stripe credit card
	 * 
	 * @param creditCardPayment
	 * @return
	 */
	public Map<String, Object> processCharge(CreditCardPayment creditCardPayment) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		resultMap.put("result", Boolean.FALSE);

		/** retrieving the stripe secrete key from database of associate id */
		Seller seller = sellerService.findSellerById(creditCardPayment.getSeller());
		
		/** TODO : do not remove this block as of now, it will be useful in feature 
		JsonObject account = StripeObject.PRETTY_PRINT_GSON.fromJson(seller.getSellerInfo(), JsonObject.class);*/
		
		Stripe.apiKey = PaymentConstants.STRIPE_API_KEY;

		Map<String, Object> chargeParams = new HashMap<String, Object>();

		/** TODO multiplying amount by 100 to convert to cents. As user will enter in $ */
		
		long cents = creditCardPayment.getAmount() * 100;
		chargeParams.put("amount", cents);
		chargeParams.put("currency", creditCardPayment.getCurrency());
		chargeParams.put("source", creditCardPayment.getStripeToken());
		chargeParams.put("description", creditCardPayment.getDescription());
		chargeParams.put("application_fee", ((Long.parseLong(creditCardPayment.getApp_fee()) * cents) / 100));

		try {
			Charge createdCharge = Charge.create(chargeParams, RequestOptions.builder().setStripeAccount(seller.getSellerInfo()).build());

			resultMap.put("result", createdCharge.getPaid());
			resultMap.put("id", createdCharge.getId());

			LOG.debug("Charge is created : " + createdCharge.getId());
		} catch (Exception e) {
			e.printStackTrace();

			resultMap.put("error", e.getLocalizedMessage());
		}

		return resultMap;
	}
	
	/**
	 * create managed account of seller
	 * 
	 * @param email
	 * @return
	 * @throws AuthenticationException
	 * @throws InvalidRequestException
	 * @throws APIConnectionException
	 * @throws CardException
	 * @throws APIException
	 */
	public Account createManagedAcount(String email) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Stripe.apiKey = PaymentConstants.STRIPE_API_KEY;

		Map<String, Object> accountParams = new HashMap<String, Object>();
		accountParams.put("managed", true);
		accountParams.put("country", "US");
		accountParams.put("email", email);
		
		LOG.debug("Going to create managed account of seller associated with email : " + email);

		return Account.create(accountParams);
	}
}
