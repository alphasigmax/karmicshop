//package com.karrmic.jobs;
//
//import org.quartz.*;
//import org.springframework.scheduling.quartz.QuartzJobBean;
//
///**
// * Created by spyder on 12/10/15.
// */
//@PersistJobDataAfterExecution
//@DisallowConcurrentExecution
//public class UpdateProducts extends QuartzJobBean{
//    public static final String COUNT = "count";
//    private String name;
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
//        JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();
//        int cnt = dataMap.getInt(COUNT);
//        JobKey jobKey = ctx.getJobDetail().getKey();
//        System.out.println(jobKey+": "+name+": "+ cnt);
//        cnt++;
//        dataMap.put(COUNT, cnt);
//    }
//}
