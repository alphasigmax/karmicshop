package com.karrmic.repository;

import com.karrmic.domain.Category;
import com.karrmic.domain.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Category entity.
 */
public interface CategoryRepository extends JpaRepository<Category,Long> {

    Page<Category> findByParentCategoryIsNull(Pageable pageable);

    @Query("select c from Category c where c.parentCategory.id =?1")
    Page<Category> findByParentCategory(Long id,Pageable pageable);
}
