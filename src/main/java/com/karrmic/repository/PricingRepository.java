package com.karrmic.repository;

import com.karrmic.domain.Pricing;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Pricing entity.
 */
public interface PricingRepository extends JpaRepository<Pricing,Long> {

}
