package com.karrmic.repository;

import com.karrmic.domain.Shopper;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Shopper entity.
 */
public interface ShopperRepository extends JpaRepository<Shopper,Long> {

    @Query("select distinct shopper from Shopper shopper left join fetch shopper.addresss")
    List<Shopper> findAllWithEagerRelationships();

    @Query("select shopper from Shopper shopper left join fetch shopper.addresss where shopper.id =:id")
    Shopper findOneWithEagerRelationships(@Param("id") Long id);

}
