package com.karrmic.repository;

import com.karrmic.domain.Seller;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Seller entity.
 */
public interface SellerRepository extends JpaRepository<Seller,Long> {

    @Query("select distinct seller from Seller seller left join fetch seller.addresss")
    List<Seller> findAllWithEagerRelationships();

    @Query("select seller from Seller seller left join fetch seller.addresss where seller.id =:id")
    Seller findOneWithEagerRelationships(@Param("id") Long id);

}
