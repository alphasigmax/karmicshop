package com.karrmic.repository;

import com.karrmic.domain.ContactInfo;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ContactInfo entity.
 */
public interface ContactInfoRepository extends JpaRepository<ContactInfo,Long> {

}
