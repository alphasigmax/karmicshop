package com.karrmic.repository.search;

import com.karrmic.domain.Deals;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Deals entity.
 */
public interface DealsSearchRepository extends ElasticsearchRepository<Deals, Long> {
}
