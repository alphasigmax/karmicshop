package com.karrmic.repository.search;

import com.karrmic.domain.Seller;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Seller entity.
 */
public interface SellerSearchRepository extends ElasticsearchRepository<Seller, Long> {
}
