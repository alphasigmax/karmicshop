package com.karrmic.repository.search;

import com.karrmic.domain.Shopper;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Shopper entity.
 */
public interface ShopperSearchRepository extends ElasticsearchRepository<Shopper, Long> {
}
