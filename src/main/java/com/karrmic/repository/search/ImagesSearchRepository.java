package com.karrmic.repository.search;

import com.karrmic.domain.Images;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Images entity.
 */
public interface ImagesSearchRepository extends ElasticsearchRepository<Images, Long> {
}
