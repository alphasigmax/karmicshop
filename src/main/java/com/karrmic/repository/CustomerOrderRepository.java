package com.karrmic.repository;

import com.karrmic.domain.CustomerOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the CustomerOrder entity.
 */
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder,Long> {


    @Query("select customerOrder from CustomerOrder customerOrder where customerOrder.orderStatus='INCART' and customerOrder.shopperUserId = :shopperid")
    Page<CustomerOrder> findShoppingCartItems(Pageable pageable, @Param("shopperid") String shopperid);

    @Query("select customerOrder from CustomerOrder customerOrder where customerOrder.orderStatus='INCART' and customerOrder.shopperUserId = ?1 and customerOrder.productId= ?2")
    List<CustomerOrder> findAllShoppingCart(String shopperid, Long productid);

}
