package com.karrmic.repository;

import com.karrmic.domain.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the City entity.
 */
public interface CityRepository extends JpaRepository<City,Long> {

    Page<City> findByCountryIsoCode(String countryCode, Pageable pageable);

    @Query("select c from City c where c.cityName like %?1%")
    Page<City> findByCity(String city, Pageable pageable);

    @Query("select c from City c where c.cityName like %?1% and c.countryIsoCode= ?2")
    Page<City> findByCityAndCountryIsoCode(String city,String countryCode, Pageable pageable);

}
