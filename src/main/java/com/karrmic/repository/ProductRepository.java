package com.karrmic.repository;

import com.karrmic.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface ProductRepository extends JpaRepository<Product,Long> {

    @Query("select c from Product c where c.category.id = ?1")
    Page<Product> findProductsByCategory(Long categoryId, Pageable pageable);
}
