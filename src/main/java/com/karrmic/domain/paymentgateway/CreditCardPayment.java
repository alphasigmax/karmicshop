package com.karrmic.domain.paymentgateway;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class CreditCardPayment extends Payment {

	private String object;
	private String number;
	private String exp_month;
	private String exp_year;
	private String cvc;
	private String address_line1;
	private String address_line2;
	private String address_city;
	private String address_zip;
	private String address_state;
	private String address_country;
	private String stripeToken;
	private String app_fee;
	
	/**
	 * @return the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * @param object
	 *            the object to set
	 */
	public void setObject(String object) {
		this.object = object;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the exp_month
	 */
	public String getExp_month() {
		return exp_month;
	}

	/**
	 * @param exp_month
	 *            the exp_month to set
	 */
	public void setExp_month(String exp_month) {
		this.exp_month = exp_month;
	}

	/**
	 * @return the exp_year
	 */
	public String getExp_year() {
		return exp_year;
	}

	/**
	 * @param exp_year
	 *            the exp_year to set
	 */
	public void setExp_year(String exp_year) {
		this.exp_year = exp_year;
	}

	/**
	 * @return the cvc
	 */
	public String getCvc() {
		return cvc;
	}

	/**
	 * @param cvc
	 *            the cvc to set
	 */
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}

	/**
	 * @return the address_line1
	 */
	public String getAddress_line1() {
		return address_line1;
	}

	/**
	 * @param address_line1
	 *            the address_line1 to set
	 */
	public void setAddress_line1(String address_line1) {
		this.address_line1 = address_line1;
	}

	/**
	 * @return the address_line2
	 */
	public String getAddress_line2() {
		return address_line2;
	}

	/**
	 * @param address_line2
	 *            the address_line2 to set
	 */
	public void setAddress_line2(String address_line2) {
		this.address_line2 = address_line2;
	}

	/**
	 * @return the address_city
	 */
	public String getAddress_city() {
		return address_city;
	}

	/**
	 * @param address_city
	 *            the address_city to set
	 */
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}

	/**
	 * @return the address_zip
	 */
	public String getAddress_zip() {
		return address_zip;
	}

	/**
	 * @param address_zip
	 *            the address_zip to set
	 */
	public void setAddress_zip(String address_zip) {
		this.address_zip = address_zip;
	}

	/**
	 * @return the address_state
	 */
	public String getAddress_state() {
		return address_state;
	}

	/**
	 * @param address_state
	 *            the address_state to set
	 */
	public void setAddress_state(String address_state) {
		this.address_state = address_state;
	}

	/**
	 * @return the address_country
	 */
	public String getAddress_country() {
		return address_country;
	}

	/**
	 * @param address_country
	 *            the address_country to set
	 */
	public void setAddress_country(String address_country) {
		this.address_country = address_country;
	}
	

	/**
	 * @return the stripeToken
	 */
	public String getStripeToken() {
		return stripeToken;
	}

	/**
	 * @param stripeToken the stripeToken to set
	 */
	public void setStripeToken(String stripeToken) {
		this.stripeToken = stripeToken;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public Map<String, Object> toMap() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("object", this.getObject());
		resultMap.put("number", this.getNumber());
		resultMap.put("exp_month", this.getExp_month());
		resultMap.put("exp_year", this.getExp_year());
		resultMap.put("cvc", this.getCvc());

		return resultMap;
	}

	/**
	 * @return the app_fee
	 */
	public String getApp_fee() {
		return app_fee;
	}

	/**
	 * @param app_fee the app_fee to set
	 */
	public void setApp_fee(String app_fee) {
		this.app_fee = app_fee;
	}
}
