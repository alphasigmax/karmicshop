package com.karrmic.domain.enumeration;

/**
 * The AddressType enumeration.
 */
public enum AddressType {
     Residential, HQ, Branch, Temporary
}
