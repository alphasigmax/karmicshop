package com.karrmic.domain.enumeration;

/**
 * The DealType enumeration.
 */
public enum DealType {
    currentweek, nextweek, pastweek, superdeal, hours24, featuredbrands, featured, haggell
}
