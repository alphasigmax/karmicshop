package com.karrmic.domain.enumeration;

/**
 * The ImageType enumeration.
 */
public enum ImageType {
     thumbnail, smallpic, largepic, hover, gallery, normal
}
