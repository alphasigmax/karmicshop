package com.karrmic.domain.enumeration;

/**
 * The DiscountType enumeration.
 */
public enum DiscountType {
    NORMAL, MEMBERONLY, LOCALONLY, HOTDEAL, DAILYDEAL, WEEKLYDEAL, MONTHLYDEAL
}
