package com.karrmic.domain.enumeration;

/**
 * The PricingType enumeration.
 */
public enum PricingType {
     NORMAL,DISCOUNTED
}
