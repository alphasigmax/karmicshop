package com.karrmic.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    INCART, STARTCHECKOUT, CHECKEDOUT, PAID, SHIPPED, RECEIVED, CLOSED
}
