package com.karrmic.domain.enumeration;

/**
 * The ImageCategory enumeration.
 */
public enum ImageCategory {
    product, seller, shopper, category, gallery
}
