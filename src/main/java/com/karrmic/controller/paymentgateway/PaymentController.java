package com.karrmic.controller.paymentgateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.karrmic.service.seller.SellerService;

/**
 * This is base controller for any payment related operations.
 * 
 * @author VishalZanzrukia
 * @see PaypalPaymentController
 * @see StripePaymentController
 */
@Controller
public class PaymentController {

	@RequestMapping(value = "/mypaymenthome", method = RequestMethod.GET)
	public String home(ModelMap model) {
		return "mypaymenthome";
    }
	
}
