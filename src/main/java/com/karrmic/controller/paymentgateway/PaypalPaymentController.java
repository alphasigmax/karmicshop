package com.karrmic.controller.paymentgateway;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;
import com.karrmic.constant.paymentgateway.PaymentConstants;

/**
 * This is PayPal controller which will handle all PayPal related operations.
 * 
 * @author VishalZanzrukia
 * @see PaymentController
 */
@Controller
public class PaypalPaymentController {

	/**
	 * logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PaypalPaymentController.class);

	/**
	 * handle page load request
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/paypaldemo", method = RequestMethod.GET)
	public ModelAndView paypaldemo(ModelMap model) {
		LOG.debug(this.getClass().getName() + " ==> Method : paypaldemo ==> Enter");

		BraintreeGateway gateway = getPaypalGateway();
		LOG.debug("Gateway Received as " + gateway);

		String clientToken = gateway.clientToken().generate();
		LOG.debug("Token Received as " + clientToken);

		ModelAndView view = new ModelAndView("paypal");
		view.addObject("clientToken", clientToken);

		return view;
	}

	/**
	 * @return BraintreeGateway
	 */
	private BraintreeGateway getPaypalGateway() {
		BraintreeGateway gateway = new BraintreeGateway(Environment.SANDBOX, PaymentConstants.PAYPAL_MERCHANTID,
				PaymentConstants.PAYPAL_PUBLICKEY, PaymentConstants.PAYPAL_PRIVATEKEY);

		return gateway;
	}

	/**
	 * Process checkout(payment) using PayPal
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.POST)
	public ModelAndView processPaypalPayment(ModelMap model, HttpServletRequest request) {
		LOG.debug(this.getClass().getName() + " ==> Method : processPaypalPayment ==> Enter");

		String nonce = request.getParameter("payment_method_nonce");
		String amount = request.getParameter("amount");

		BraintreeGateway gateway = getPaypalGateway();
		LOG.debug("Gateway Received as " + gateway);

		TransactionRequest trRequest = new TransactionRequest().amount(new BigDecimal(amount))
				.paymentMethodNonce(nonce);
		Result<Transaction> result = gateway.transaction().sale(trRequest);

		Map<String, Object> resultMap = new HashMap<String, Object>();

		Boolean finalResult = Boolean.FALSE;
		if (result.isSuccess()) {
			LOG.debug("Received Success ");
			Transaction transaction = result.getTarget();

			finalResult = Boolean.TRUE;
			resultMap.put("id", transaction.getId());
			resultMap.put("status", transaction.getStatus());
			resultMap.put("responsecode", transaction.getProcessorResponseCode());
			resultMap.put("responsetext", transaction.getProcessorResponseText());
		} else {
			LOG.debug("Received Failure");
			if (result.getTransaction() != null) {
				Transaction transaction = result.getTransaction();

				resultMap.put("id", transaction.getId());
				resultMap.put("status", transaction.getStatus());
				resultMap.put("responsecode", transaction.getProcessorResponseCode());
				resultMap.put("responsetext", transaction.getProcessorResponseText());

			}

			if (result.getErrors() != null) {
				for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
					resultMap.put("Code", error.getCode());
					resultMap.put("ErrorMessage", error.getMessage());
					resultMap.put("Attribute", error.getAttribute());
				}
			}
		}

		resultMap.put("result", finalResult);

		ModelAndView view = new ModelAndView("paypalPaymentStatus");

		Set<Entry<String, Object>> entrySet = resultMap.entrySet();
		LOG.debug("Following parameters will be shown to end user");
		for (Entry<String, Object> entry : entrySet) {
			LOG.debug("Key " + entry.getKey() + " Value " + entry.getValue());
		}
		
		LOG.debug("========================Paypal Checkout done=============================");

		view.addObject("resultMap", resultMap);

		return view;
	}
}
