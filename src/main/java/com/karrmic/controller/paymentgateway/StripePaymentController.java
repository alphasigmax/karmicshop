package com.karrmic.controller.paymentgateway;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.karrmic.domain.paymentgateway.CreditCardPayment;
import com.karrmic.service.paymentgateway.StripeService;
import com.karrmic.service.seller.SellerService;

/**
 * This is stripe payment controller.
 * 
 * @author VishalZanzrukia
 *
 */
@Controller
public class StripePaymentController {

	private static final Logger LOG = LoggerFactory.getLogger(StripePaymentController.class);
	
	@Autowired
	private StripeService stripeService;
	
	@Autowired
	private SellerService sellerService;
	
	@RequestMapping(value = "/stripedemo", method = RequestMethod.GET)
	public String stripedemo(ModelMap model) {
		model.addAttribute("sellers", sellerService.getAllSellers());
		return "demo";
    }

	@RequestMapping(value = "/processStripPayment", method = RequestMethod.POST)
	public View processPayment(@ModelAttribute CreditCardPayment creditCardPayment, ModelMap model,
			RedirectAttributes redirectAttributes, HttpServletRequest request)throws IOException {

		LOG.debug("Request Received as \n " + creditCardPayment.toString());

		Map<String, Object> resultMap = stripeService.processCharge(creditCardPayment);
		LOG.info("Result received from Payment Service " + resultMap);

		redirectAttributes.addFlashAttribute("resultMap", resultMap);
		return new RedirectView("/paymentStatus", true);
	}

	@RequestMapping(value = "/paymentStatus", method = RequestMethod.GET)
	public String passwordPage(ModelMap model) {
		return "paymentStatus";
	}
}
