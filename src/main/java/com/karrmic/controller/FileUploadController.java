package com.karrmic.controller;

import com.karrmic.domain.Images;
import com.karrmic.domain.enumeration.ImageCategory;
import com.karrmic.repository.ImagesRepository;
import com.karrmic.repository.search.ImagesSearchRepository;
import com.karrmic.web.rest.util.HeaderUtil;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;

import javax.imageio.ImageIO;
/**
 * Created by spyder on 13/10/15.
 */

@Controller
@RequestMapping(value = "/upload")
public class FileUploadController {
    private final Logger log = LoggerFactory.getLogger(FileUploadController.class);
//    private static final String localImagePath = "/home/spyder/imagesdir/";
    private static final String localImagePath = "/tmp/";
    @Inject
    private ImagesRepository imagesRepository;

    @Inject
    private ImagesSearchRepository imagesSearchRepository;


    @RequestMapping(value = "/uploadBatch", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Images> saveBatch(@RequestParam("file") MultipartFile file, @RequestParam("imagecategory") ImageCategory imageType) {
        log.debug("Uploading file:" );
        //get the local user and store in his directory
        String fileName ;
        if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();

                File absoluteFile = new File(localImagePath + fileName);
                byte[] bytes = file.getBytes();

                InputStream in = new ByteArrayInputStream(bytes);
                BufferedImage image = ImageIO.read(in);

                //thumbnail
                BufferedImage thumbnail = Scalr.resize(image, 150);

                //other sizes
                BufferedOutputStream buffStream =
                    new BufferedOutputStream(new FileOutputStream(absoluteFile));
                buffStream.write(bytes);
                buffStream.close();
                Images images = new Images();
                images.setImagePath(absoluteFile.getPath());
                images.setImageCategory(imageType);
                images.setCategoryId(-1L);
                Images result = imagesRepository.save(images);
                imagesSearchRepository.save(result);
                return ResponseEntity.created(new URI("/api/imagess/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert("images", result.getId().toString()))
                    .body(result);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            log.debug("Invalid file uploaded" );
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Images> singleSave(@RequestParam("file") MultipartFile file, @RequestParam("imagecategory") ImageCategory imageType) {
        log.debug("Uploading file:" );
        String fileName ;
        if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();
                File absoluteFile = new File(localImagePath + fileName);
                byte[] bytes = file.getBytes();
                BufferedOutputStream buffStream =
                    new BufferedOutputStream(new FileOutputStream(absoluteFile));
                buffStream.write(bytes);
                buffStream.close();
                Images images = new Images();
                images.setImagePath(absoluteFile.getPath());
                images.setImageCategory(imageType);
                images.setCategoryId(-1L);
                Images result = imagesRepository.save(images);
                imagesSearchRepository.save(result);
                return ResponseEntity.created(new URI("/api/imagess/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert("images", result.getId().toString()))
                    .body(result);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            log.debug("Invalid file uploaded" );
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/multipleSave", method = RequestMethod.POST)
    public
    @ResponseBody
    String multipleSave(@RequestParam("file") MultipartFile[] files) {
        String fileName = null;
        String msg = "";
        if (files != null && files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                try {
                    fileName = files[i].getOriginalFilename();
                    byte[] bytes = files[i].getBytes();
                    BufferedOutputStream buffStream =
                        new BufferedOutputStream(new FileOutputStream(new File(localImagePath + fileName)));
                    buffStream.write(bytes);
                    buffStream.close();
                    msg += "You have successfully uploaded " + fileName + "<br/>";
                } catch (Exception e) {
                    return "You failed to upload " + fileName + ": " + e.getMessage() + "<br/>";
                }
            }
            return msg;
        } else {
            return "Unable to upload. File is empty.";
        }
    }
}
