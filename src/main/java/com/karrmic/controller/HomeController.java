package com.karrmic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by spyder on 4/10/15.
 */
@Controller
public class HomeController {

   @RequestMapping(value = "/")
    public String homePage() {

       return "/app/index";
    }

    @RequestMapping(value = "/homenew")
    public String homeNewOnePage() {

        return "/home_new";
    }


    @RequestMapping(value = "/main")
    public String homeNewPage() {

        return "/newapp/index";
    }



    @RequestMapping(value = "/dashboard")
    public String homeDashPage() {

        return "/newapp/dashboard2";
    }





    @RequestMapping(value = "/hipster")
    public String hipsterPage() {

        return "/index";
    }


}
