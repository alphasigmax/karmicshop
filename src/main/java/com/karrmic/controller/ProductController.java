package com.karrmic.controller;

import com.karrmic.domain.Product;
import com.karrmic.repository.ProductRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;

/**
 * Created by spyder on 4/10/15.
 */

@Controller
public class ProductController {

    @Inject
    private ProductRepository productRepository;

    @RequestMapping(value = "/product/{id}", method= RequestMethod.GET)
    public ModelAndView homePage(@PathVariable("id") Long productid) {
      Product product=  productRepository.findOne(productid);
        return new ModelAndView("product_details", "product", product);
    }

}
