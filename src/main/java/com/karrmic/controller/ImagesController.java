package com.karrmic.controller;

import com.karrmic.domain.Images;
import com.karrmic.repository.ImagesRepository;
import com.karrmic.web.rest.ImagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

/**
 * Created by spyder on 7/12/15.
 */

@Controller
@RequestMapping(value = "/images")
public class ImagesController {


    private final Logger log = LoggerFactory.getLogger(ImagesResource.class);

    @Inject
    private ImagesRepository imagesRepository;
    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> doit(@PathVariable("id") Long imageid) throws Exception
    {
      Images images= imagesRepository.findOne(imageid);
        if(null!=images) {
            File gridFsFile = new File(images.getImagePath());
            return ResponseEntity.ok()
                .contentLength(gridFsFile.length())
                .contentType(MediaType.parseMediaType(Files.probeContentType(gridFsFile.toPath()) ))
                .body(new InputStreamResource(new FileInputStream(gridFsFile)));
        }
        else
        {
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
