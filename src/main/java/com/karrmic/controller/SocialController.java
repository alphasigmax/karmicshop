package com.karrmic.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.karrmic.domain.util.SocialControllerUtil;


@Controller
public class SocialController {

    private static final Logger LOG = LoggerFactory.getLogger(SocialController.class);

    @Autowired
    private ConnectionRepository connectionRepository;


    @Autowired
    private SocialControllerUtil util;

    /**
     * Rediret to welcome screen
     * 
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping("/welcome")
    public String home(HttpServletRequest request, Principal currentUser, Model model) {
        util.setModel(request, currentUser, model);
        return "welcome";
    }

    
    /**
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping(method=RequestMethod.POST,value="/logout")
    public ModelAndView logout(HttpServletRequest request, Principal currentUser, Model model) {
        util.setModel(request, currentUser, model);
        
        request.getSession().invalidate();
        
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(new RedirectView("app/login#/access/login"));
        return modelAndView;
    }
    
    @RequestMapping("/app/login")
    public String appLogin(HttpServletRequest request, Principal currentUser, Model model) {
        util.setModel(request, currentUser, model);
        return "app/login";
    }

   

}
