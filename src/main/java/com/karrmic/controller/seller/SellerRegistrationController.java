package com.karrmic.controller.seller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elasticsearch.common.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.karrmic.controller.paymentgateway.StripePaymentController;
import com.karrmic.domain.Seller;
import com.karrmic.service.paymentgateway.StripeService;
import com.karrmic.service.seller.SellerService;
import com.stripe.model.Account;

/**
 * This is seller registration controller.
 * 
 * @author VishalZanzrukia
 *
 */
@Controller
@RequestMapping("/seller/registration")
public class SellerRegistrationController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StripePaymentController.class);
	
	@Autowired
	private SellerService sellerService;
	
	@Autowired
	private StripeService stripeService;
	
	/**
	 * to bind date
	 * 
	 * @param binder
	 */
	@InitBinder
	public void bindingPreparation(WebDataBinder binder) {
	  DateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY");
	  CustomDateEditor orderDateEditor = new CustomDateEditor(dateFormat, true);
	  binder.registerCustomEditor(DateTime.class, orderDateEditor);
	}

	/**
	 * get registration page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String stripedemo(ModelMap model) {
		return "seller/registration";
    }
	
	/**
	 * post registration form
	 * 
	 * @param model
	 */
	@RequestMapping(method = RequestMethod.POST)
	public View registerSeller(@ModelAttribute Seller seller, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		try{
			/** registering the managed account of seller */
			Account account = stripeService.createManagedAcount(seller.getEmailAddress());
			
			LOG.debug("Seller account has been created as : " + account);
			
			/**TODO: do't remove below lines as of now			
			 * seller.setSellerInfo(StripeObject.PRETTY_PRINT_GSON.toJson(account)); */
			
			/** retrieving the id associated with seller account */
			seller.setSellerInfo(account.getId());
			sellerService.save(seller);
			
			LOG.debug("Seller managed account has been created successfully associated with " + seller.getEmailAddress());
			redirectAttributes.addFlashAttribute("success", "You are succesfully Registered.");
		}
		catch(Exception e){
			redirectAttributes.addFlashAttribute("error", e.getLocalizedMessage());
		}
		return new RedirectView("/seller/registration", true);
    }
}
