buildscript {
    repositories {
        mavenLocal()
        jcenter()
        maven { url 'http://repo.spring.io/plugins-release' }
    }
    dependencies {
        classpath group: 'org.springframework.boot', name: 'spring-boot-gradle-plugin', version: spring_boot_version
        classpath group: 'org.springframework.build.gradle', name: 'propdeps-plugin', version: '0.0.7'
        classpath group: 'com.moowork.gradle', name: 'gradle-node-plugin', version: '0.11'
        classpath group: 'com.moowork.gradle', name: 'gradle-grunt-plugin', version: '0.11'
    }
}

apply plugin: 'java'
sourceCompatibility=1.8
targetCompatibility=1.8
apply plugin: 'maven'
apply plugin: 'spring-boot'
apply plugin: 'war'
apply plugin: 'propdeps'
defaultTasks 'bootRun'
apply plugin: 'eclipse'
apply plugin: 'idea'
bootRepackage {
   mainClass = 'com.karrmic.Application'
}

springBoot {
    mainClass = 'com.karrmic.Application'
}

bootRun {
    addResources = false
}

apply from: 'yeoman.gradle'
apply from: 'sonar.gradle'

apply from: 'liquibase.gradle'
apply from: 'gatling.gradle'

apply from: 'mapstruct.gradle'

if (project.hasProperty('prod')) {
    apply from: 'profile_prod.gradle'
} else if (project.hasProperty('fast')) {
    apply from: 'profile_fast.gradle'
} else {
  apply from: 'profile_dev.gradle'
}

group = 'com.karrmic'
version = '0.0.1-SNAPSHOT'

description = ''

configurations {
    providedRuntime
}

repositories {
    maven { url 'http://repo.spring.io/milestone' }
    maven { url 'http://repo.spring.io/snapshot' }
    maven { url 'https://repository.jboss.org/nexus/content/repositories/releases' }
    maven { url 'https://oss.sonatype.org/content/repositories/releases' }
    maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
    maven { url 'http://repo.maven.apache.org/maven2' }
}

configurations.all {
  resolutionStrategy {
    // force certain versions of dependencies (including transitive)
    //  *append new forced modules:
    force group: 'com.fasterxml.jackson.core', name: 'jackson-core', version: jackson_version
    force group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: jackson_version
    force group: 'joda-time', name: 'joda-time', version: joda_time_version

  }
}

dependencies {
    compile group: 'io.dropwizard.metrics', name: 'metrics-core'
    compile group: 'io.dropwizard.metrics', name: 'metrics-annotation', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-ehcache', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-graphite', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-healthchecks', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-jvm', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-servlet', version: dropwizard_metrics_version
    compile group: 'io.dropwizard.metrics', name: 'metrics-json', version: dropwizard_metrics_version
    compile (group: 'io.dropwizard.metrics', name: 'metrics-servlets', version: dropwizard_metrics_version) {
        exclude(module: 'metrics-healthchecks')
    }
    compile group: 'com.fasterxml.jackson.datatype', name: 'jackson-datatype-json-org', version: jackson_version
    compile group: 'com.fasterxml.jackson.datatype', name: 'jackson-datatype-hppc', version: jackson_version
    compile group: 'com.fasterxml.jackson.datatype', name: 'jackson-datatype-joda', version: jackson_version
    compile group: 'com.fasterxml.jackson.datatype', name: 'jackson-datatype-hibernate4', version: jackson_version
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-annotations', version: jackson_version
    compile (group: 'com.ryantenney.metrics', name: 'metrics-spring', version: metrics_spring_version) {
        exclude(module: 'metrics-core')
        exclude(module: 'metrics-healthchecks')
    }
    compile(group: 'com.zaxxer', name: 'HikariCP', version: HikariCP_version) {
        exclude(module: 'tools')
    }
    compile group: 'commons-lang', name: 'commons-lang', version: commons_lang_version
    compile group: 'commons-io', name: 'commons-io', version: commons_io_version
    compile group: 'javax.inject', name: 'javax.inject', version: javax_inject_version
    compile group: 'javax.transaction', name: 'javax.transaction-api'
    compile group: 'joda-time', name: 'joda-time', version: joda_time_version
    compile group: 'joda-time', name: 'joda-time-hibernate', version: joda_time_hibernate_version
    compile group: 'org.apache.geronimo.javamail', name: 'geronimo-javamail_1.4_mail', version: geronimo_javamail_1_4_mail_version
    compile group: 'org.hibernate', name: 'hibernate-core', version: hibernate_entitymanager_version
    compile group: 'org.hibernate', name: 'hibernate-envers'
    compile (group: 'org.hibernate', name: 'hibernate-ehcache') {
        exclude(module: 'ehcache-core')
    }
    compile group: 'org.hibernate', name: 'hibernate-validator'
    compile group: 'org.jadira.usertype', name: 'usertype.core', version: usertype_core_version
    compile (group: 'org.liquibase', name: 'liquibase-core', version: liquibase_core_version) {
        exclude(module: 'jetty-servlet')
    }
    compile group: 'com.mattbertolini', name: 'liquibase-slf4j', version: liquibase_slf4j_version
    compile group: 'org.springframework.boot', name: 'spring-boot-actuator'
    compile group: 'org.springframework.boot', name: 'spring-boot-autoconfigure'
    compile group: 'org.springframework.boot', name: 'spring-boot-loader-tools'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-logging'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-aop'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-data-jpa'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-data-elasticsearch'
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-security'
    compile(group: 'org.springframework.boot', name: 'spring-boot-starter-web') {
        exclude module: 'spring-boot-starter-tomcat'
    }
    compile group: 'org.springframework.boot', name: 'spring-boot-starter-thymeleaf'
    //  compile 'org.thymeleaf:thymeleaf:3.0.0.BETA01'
    // compile 'org.thymeleaf:thymeleaf-spring4:3.0.0.BETA01'

    compile group: 'org.springframework.cloud', name: 'spring-cloud-cloudfoundry-connector'
    compile group: 'org.springframework.cloud', name: 'spring-cloud-spring-service-connector'
    compile group: 'org.springframework.cloud', name: 'spring-cloud-localconfig-connector'
    compile(group: 'org.springframework', name: 'spring-context-support') {
        //  exclude(module: 'quartz')
    }
    compile group: 'org.springframework.security', name: 'spring-security-config', version: spring_security_version
    compile group: 'org.springframework.security', name: 'spring-security-data', version: spring_security_version
    compile group: 'org.springframework.security', name: 'spring-security-web', version: spring_security_version
    compile(group: 'io.springfox', name: 'springfox-swagger2', version: springfox_version){
        exclude module: 'mapstruct'
    }

    compile group: 'mysql', name: 'mysql-connector-java'
    compile group: 'com.h2database', name: 'h2'
    compile group: 'fr.ippon.spark.metrics', name: 'metrics-spark-reporter', version: metrics_spark_reporter_version
    compile group: 'org.mapstruct', name: 'mapstruct-jdk8', version: mapstruct_version
    testCompile group: 'com.jayway.awaitility', name: 'awaitility', version: awaility_version
    testCompile group: 'com.jayway.jsonpath', name: 'json-path'
    testCompile group: 'org.springframework.boot', name: 'spring-boot-starter-test'
    testCompile group: 'org.springframework.boot', name: 'spring-boot-starter-jetty'
	testCompile group: 'org.assertj', name: 'assertj-core', version: assertj_core_version
    testCompile group: 'junit', name: 'junit'
    testCompile group: 'org.mockito', name: 'mockito-core'
    testCompile group: 'com.mattbertolini', name: 'liquibase-slf4j', version: liquibase_slf4j_version
    testCompile group: 'org.hamcrest', name: 'hamcrest-library'
    testCompile group: 'io.gatling.highcharts', name: 'gatling-charts-highcharts', version: gatling_version

    //payment related stuff
    compile("com.stripe:stripe-java:1.37.0")
    compile("com.braintreepayments.gateway:braintree-java:2.49.0")
    compile("net.sourceforge.nekohtml:nekohtml:1.9.21")
    compile("commons-lang:commons-lang:2.6")

    //compile ("org.hsqldb:hsqldb")
    compile("commons-dbcp:commons-dbcp:1.4")

 compile ("org.imgscalr:imgscalr-lib:4.2")
    compile("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.boot:spring-boot-starter-actuator")
    compile("org.springframework.boot:spring-boot-starter-data-jpa")
    compile("org.springframework.boot:spring-boot-starter-security")
    compile("org.springframework.boot:spring-boot-starter-thymeleaf")
    compile("org.springframework.social:spring-social-security:1.1.2.RELEASE")
    compile("org.springframework.social:spring-social-config:1.1.2.RELEASE")
    compile("org.springframework.social:spring-social-facebook:1.1.0.RELEASE")
    compile("org.springframework.social:spring-social-twitter:1.1.0.RELEASE")
    compile("org.springframework.social:spring-social-google:1.0.0.RELEASE")
    compile("org.apache.commons:commons-lang3:3.3.2")
    compile("com.google.guava:guava:14.0.1")
   // compile("com.h2database:h2:1.3.175")


    optional group: 'org.springframework.boot', name:'spring-boot-configuration-processor', version: spring_boot_version

}

compileJava.dependsOn(processResources)

clean {
    delete "target"
}

task wrapper(type: Wrapper) {
    gradleVersion = '2.9'
}

task stage(dependsOn: 'bootRepackage') {
}
